---
title: "Today I Learned 2021"
date: "2022-01-01"
lastmod: "2023-12-29"
tags: ["TIL"]
keywords: ["TIL"]
description: "Random facts I learned in 2021."
categories: ["today i learned"]
featured: false
draft: false
---

I'm going to start keeping a list of random facts I learn. I'm primarily recording natural organic discoveries, e.g. from conversations with real people in real life, so most entries will have a personal story behind it, although I'm not recording those stories so it'll be an exercise for the reader to imagine. There are actually some from before 2021 to make up for the fact that I decided to do this a little after the start of 2021.

## 4 Aug 2018: Some roads cross runways

For example, the A970 in Shetland crosses the runway of the main airport (Sumburgh Airport). Access is controlled by a level crossing with barriers closed whenever a flight is taking off or landing. If you hire a car at the airport you get to drive over the runway (when the barriers are open).

## 1 Nov 2019: Nomophobia is a fear of being without a working mobile phone

e.g. due to loss of phone, poor signal or low battery. It is more of a psychological condition, and proper treatment is to address the root cause rather than the symptoms, i.e. it is to ensure users can survive without a mobile phone rather than to ensure they are never without a working mobile phone (like a drug addiction is better treated by weaning the user off the drug rather than by ensuring they are always well supplied via a dealer on every street corner).

## 10 Nov 2020: There's a breed of sheep on a Scottish island that has evolved to eat mostly seaweed

Apparently it gives the meat a "unique, rich flavour". Source: [https://en.wikipedia.org/wiki/North_Ronaldsay_sheep](https://en.wikipedia.org/wiki/North_Ronaldsay_sheep).

## 21 Dec 2020: My children make pretend phone calls in a very different way from me

(Not so much Today I Learned, but Today I Noticed.) My children place flat palms towards their face, like a smart-phone, whereas I (and I believe most people my age) make pretend phone calls with a fist by their cheek and little finger out towards the mouth and thumb out towards the ear, like a traditional telephone handset.

## 12 Feb 2021: Mars has 2 moons - Phobos and Demios

(I suspect I knew this as a child but subsequently forgot.) [https://en.wikipedia.org/wiki/Mars#Moons](https://en.wikipedia.org/wiki/Mars#Moons) 

## 13 Mar 2021: There's a type of shark which has jaws that extend outwards when feeding

It is called the goblin shark, and inspired the alien jaws in Alien Covenant: [https://en.wikipedia.org/wiki/Goblin_shark](https://en.wikipedia.org/wiki/Goblin_shark). 

## 13 Mar 2021: The phrase "wild goose chase" was popularised by Shakespeare's Romeo and Juliet

Although the phrase itself may have pre-dated the play: [https://en.wiktionary.org/wiki/wild-goose_chase#English](https://en.wiktionary.org/wiki/wild-goose_chase#English).

## 3 June 2021: Dogs only see in blue and yellow

## 15 June 2021: The time dilation effect of special relativity increases exponentially towards c 

(c being the speed of light). What this means is that the "travel time" (i.e. the time experienced by the traveller) only starts shortening dramatically as the speed of light is reached, while the "proper time" (i.e. for a non-traveller) doesn't change as much. For example, to travel the 4.246 light years to the nearest star (Proxima Centauri), the "travel time" would be 4 years at 90% of the speed of light, 1 year at 99%, 131 days at 99.9%, 41 days at 99.99%, 13 days at 99.999%, etc., while the "proper time" would be 8.9 years at 90%, 8.1 years at 99%, 8.0 years at 99.9%, 8.0 years at 99.99%, 8.0 years at 99.999%, etc.: [https://www.emc2-explained.info/Time-Dilation/](https://www.emc2-explained.info/Time-Dilation/)

## 30 June 2021: An "exonumist" is a person who collects exonumia

Exonumia are numismatic items other than legal tender coins and paper money, e.g. tokens, souvenir medallions, casino chips.

## 2 Jul 2021: The phrase "gardyloo" was only used in Scotland

The phrase "[gardyloo](https://en.wiktionary.org/wiki/gardyloo)" was shouted in medieval times to warn those below that toilet waste was about to be thrown out of the window. I learned the phrase at school, and have periodically told others of it since. I had always assumed it was used UK-wide, but apparently it was only used in Scotland. I wonder what they said in England. Or maybe they didn't say anything to warn those below before throwing their toilet waste out of the window. (And yes, the English also threw toilet waste out of the window in medieval times.) While I'm here, I learned a few weeks back that "squint" in England only means to narrow one's eyes, whereas in Scotland it can also mean wonky or askew, so all the times in England I've said something like "the picture's a bit squint", the English won't have understood what I meant.

## 10 Jul 2021: The darker outer ring some people have in their eyes is called a "limbal ring"

## 16 Aug 2021: Some words in some languages require a different way of thinking

And so don't have direct translations. As an example: "Gaelic colours reflect the quality of light and natural environment of the islands and do not always correspond to English names" [https://www.visitouterhebrides.co.uk/see-and-do/gaelic-for-sailors-colours-p545611](https://www.visitouterhebrides.co.uk/see-and-do/gaelic-for-sailors-colours-p545611).

## 18 Aug 2021: Afghanistan shares a small border with China

## 6 Sep 2021: There aren't any photos of Neil Armstrong on the moon

Buzz Aldrin had a camera and took lots of photos, but didn't think to take any with Neil in. Neil also had a camera, but thought to take photos with Buzz in.

## 16 Sep 2021: Sign language includes some visual puns

e.g. the sign for pasteurised milk involves moving ones hand "past your eyes".

## 22 Oct 2021: Marshmallows on a hot chocolate aren't just flavouring - they also serve a function

The marshmallows act as an insulating layer, allowing you to put squirty cream on the hot drink without it just melting and sinking.

## 1 Nov 2021: Hallow is an archaic term for a saint or holy person

Hence All Hallows Day also being known as All Saints Day.

## 17 Nov 2021: The odour added to natural gas in the UK no longer smells like rotting vegetables

Natural gas, as piped to many UK homes, is odourless, but "odourised" to help people detect leaks. The original odouriser used in the UK (THT) was often confused with rotting vegetables though, so a new odouriser was introduced (80% TBM 20% DMS) which has a very distinctive odour and is therefore difficult to confuse with anything else.

## 18 Dec 2021: The copyright to the song "Happy Birthday To You" was declared invalid in 2015

So all those jokes in cartoons about "we can't afford to licence the 'Happy Birthday To You'" song pre-date 2015. In Europe would have expired in 2017 anyway.

## 28 Dec 2021: The play Twelfth Night is called Thirteenth Night (Trettondagsafton) in Swedish

Because Christmas is celebrated on Christmas Eve.


