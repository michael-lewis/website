---
title: "searchmysite.net now has its own blog"
date: "2020-11-21"
lastmod: "2020-11-21"
tags: ["search", "search as a service", "searchmysite.net"]
keywords: ["search", "search as a service", "searchmysite.net"]
description: "I have set up a new blog at https://blog.searchmysite.net/ for the searchmysite.net posts"
categories: ["searchmysite.net"]
draft: true
---



I've set up a new blog at [https://blog.searchmysite.net/](https://blog.searchmysite.net/) for the searchmysite.net posts, given there's quite a bit I want to write on both, and searchmysite.net does seem to be starting to get a life of its own now. You can read more about how the new blog was set up at [https://blog.searchmysite.net/posts/searchmysite.net-now-has-its-own-blog/](https://blog.searchmysite.net/posts/searchmysite.net-now-has-its-own-blog/).

I've kept the existing posts about searchmysite.net here in case people have links, but copied them to the new blog for completeness, changing as little as possible in the process (mainly just changing links).

If you've an RSS reader, you might want to add [https://blog.searchmysite.net/index.xml](https://blog.searchmysite.net/index.xml).

