---
title: "Growing a cherry tree on a north-facing balcony 2022-2024"
date: "2024-07-07"
lastmod: "2024-07-07"
tags: ["cherry tree", "balcony", "plants"]
keywords: ["cherry tree", "balcony", "plants"]
description: "My continuing struggle to grow a cherry tree on a north facing balcony, this time from 2022 to 2024"
categories: ["plants"]
featured: false
draft: false
---


## Introduction

I've probably spent way too much time writing about cherry trees already (see [Growing a cherry tree on a north-facing balcony 2019-2021](/posts/growing-a-cherry-tree-on-a-north-facing-balcony-2019-2021/)). That said, I'd like to quickly jot down a few more notes, before hopefully wrapping up the topic once and for all:-)


## 2022

We had a new roof installed in 2022, which was a big job involving scaffolding all the way up both sides of the building for several months. That of course meant we couldn't keep the tree on the balcony. Fortunately I was able to put the cherry tree in the front patio of the ground floor flat. It was a nice sheltered south facing spot, with enough passing traffic to keep the birds mostly away, and the cherry tree really thrived there:

![Cherries ripening, 9 June 2022](/images/posts/cherrytree/cherrytree20220609_sm.jpg)

I moved the tree to our living room in mid-June so the cherries could finish ripening without birds or passers-by (although with regular complaints from my wife instead). It was the best crop ever.
 
The lesson from 2022 is that a good sunny and sheltered location really does help a cherry tree.


## 2023

Although it was back on the north facing balcony in 2023, it was still a reasonable crop, and I netted the tree to keep out the birds. Unfortunately I didn't get chance to harvest the cherries before our trip to the [Scottish Highlands and the Isle of Mull](/posts/london-to-the-isle-of-mull-in-a-4-year-old-electric-car/), but I thought they'd last another week on the tree. It turns out that I was wrong. Although the birds didn't get them through the nets, the wasps did:

![What cherries look like after wasps have got to them, 30 Jul 2023](/images/posts/cherrytree/cherrytree20230730_sm.jpg)

While we were away, the wasps even built themselves a large nest under the eaves of our new roof so they could more easily access the tree.

The lesson from 2023 is that cherries need to be picked within a week or so of ripening, or they will shrivel and be eaten by wasps.


## 2024

I decided not to net the tree in 2024. Big mistake. The birds plundered the fruits of my labour[^note1] and completely ravaged the tree. The big birds were the worst, showing no respect for nature, tearing the leaves to pieces with their claws as they tried to shuffle down the branches, and even breaking branches which weren't strong enough to support their excessive cherry-fed fatness. The smaller birds simply cleared the cherries the bigger birds couldn't reach.

![A pigeon caught red-handed, so to speak, 19 June 2023](/images/posts/cherrytree/cherrytree20240619_sm.jpg)

When I was young we had a cherry tree in our garden, and the birds just took what they needed and left plenty for us. We also used to pick wild cherries, along with other wild fruit. Nowadays all the wild fruit trees are stripped bare long before anything has the chance to ripen. I don't think it is due to my moving from Scotland to England[^note2], but I think it has been something that has happened in recent decades, given that I've heard many other people complain that trees even in London which once had plenty of fruit are now bare long before ripening, and also read news stories like [Pick-your-own event off as fruit eaten by birds](https://www.bbc.co.uk/news/articles/cd1n26kj1eeo). Makes you start to wonder if that post-scarcity utopia of the science fiction in my youth is becoming an ever more distant dream.

Anyway, the lesson from 2024 is that cherry trees really do need to be netted.


[^note1]: Watering, feeding, pruning, and of course dealing with pests.

[^note2]: In other words, I don't think it is a case of socially responsible wealth-sharing Scottish birds versus greedy wealth-stripping English birds.

