---
title: "Today I Learned 2024"
date: "2025-01-01"
lastmod: "2025-01-01"
tags: ["TIL"]
keywords: ["TIL"]
description: "Random facts I learned in 2024."
categories: ["today i learned"]
featured: false
draft: false
---

This is a followup to [Today I Learned 2021](/posts/today-i-learned-2021/), [Today I Learned 2022](/posts/today-i-learned-2022/) and [Today I Learned 2023](/posts/today-i-learned-2023/). As a reminder, these are serendipitous discoveries rather than the result of a dedicated search for new knowledge. Looking back, many are from places I've visited or major news stories, although the ones which appear a bit random must have been related to something I was doing at the time.

## 5 Feb 2024: A sapiophile is someone who is primarily attracted to intelligence

It isn't just being attracted to actual intelligence (because there are many situations where that isn't possible to accurately gauge), it also means being attracted to proxies for (i.e. potential indicators of) intelligence. I remember realising I identified as this many years ago (which is a whole other story). Finding it has a name is great because it indicates there are others who identify in the same way. Source: [sapiophile](https://en.wiktionary.org/wiki/sapiophile).

## 13 Feb 2024: Old recipes say to break eggs into a cup first, because of the risk of bad eggs

If you had a bad egg and it went into a cup you could safely dispose of it, while if you had a bad egg and it went into the bowl with all the other ingredients you would have to dispose of all the other ingredients too. Makes sense. Personally not encountered a bad egg before, so maybe it is less of a concern nowadays. Oh, vaguely related point, but on the question of whether eggs should be kept in the fridge or not, I heard that the main thing is not keeping them chilled as such but keeping them at a constant temperature. In the old days that might have been in a pantry, but nowadays with central heating and so on there are few indoor places that stay a reasonably constant temperature, in which case the fridge is the best place (although ironically perhaps not the egg rack in the fridge door which is exposed to more temperature variations as the door is repeatedly opened and closed).

## 15 Feb 2024: "Under the weather" likely comes from when unwell sailors went below deck

There seems to be a lot of online debate about whether it was originally "under the weather bow" or "under the weather rail" or neither of those, which makes me concerned about the validity of any online research, but it still seems reasonable that it is an old seafaring term and involved some kind of shelter from the weather below deck.

## 27 Feb 2024: A gongoozler is a person who enjoys watching activity on the canals

The term is also used more generally to describe those who harbour an interest in canals and canal life, but do not actively participate: [Gongoozler](https://en.wikipedia.org/wiki/Gongoozler).

## 7 Mar 2024: Wallsend Metro station (near Newcastle) has bilingual notices in English and Latin

Sources variously say it is the only station in the UK, or the world, with this distinction. It has Latin due to its proximity to the end of Hadrian's Wall. Which I'm assuming is where the name Wallsend comes from.

## 12 Mar 2024: In the nineteenth century one town (Redditch) produced 90% of the world's needles 
Source: [Forge Mill Needle Museum](https://www.forgemill.org.uk/web/). By the end of the 19th century "one of the largest in the area" was Abel Morrall, which was run by George Lewis followed by his son Willibrord Lewis, so you could say that they were responsible for a sizeable percentage of the world's needle production. Source: [Abel Morrall: History](http://coulthart.com/avery/company-pages/Morrall-history.htm).

## 15 Mar 2024: Whales have the biggest brain of any animals

A sperm whale has brains weighing up to 9kg, while the average human brain weighs around 1.4kg. So humans aren't set apart from animals by our brain size alone.

## 15 May 2024: Honey from rhododendrons was used as an early form of biological weapon

Source: [Mad honey](https://en.wikipedia.org/wiki/Mad_honey).

## 9 Jul 2024: "Casu martzu" is a type of cheese from Sardinia which contains live maggots

And is now banned in the EU (although still apparently available on the black market). Source: [Casu martzu](https://en.wikipedia.org/wiki/Casu_martzu).

## 25 Jul 2024: Canaries in coal mines were to detect "blackdamp" rather than "firedamp"

"Firedamp" is methane, which becomes explosive when it reaches around 5% in volume, and typically triggers a much bigger explosion with the coal dust. "Blackdamp" on the other hand is carbon monoxide, which is not explosive but asphyxiates. Source: [National Coal Mining Museum for England](https://www.ncm.org.uk/).

## 21 Aug 2024: Maps at Japanese railway stations don't always have north at the top

I usually get my bearings when I come out of unfamiliar stations by looking at the maps they tend to have by the exits, but I got a little disorientated the first couple of times in Japan, until I realised it was because the maps didn't have north to the top. At first I thought they all had east to the top, because that was what I saw with the first two couple of maps, but others later on seemed to have different directions at the top. Not sure what the story is behind this one.

## 24 Aug 2024: The old and new Japanese capitals (Kyoto and Tokyo) are the same syllables reversed

The kanji for the "kyo" part of both words is the same, but slightly different for the "to" part in both words, indicating slightly different pronunciation and meaning. The "kyo" means "capital", and the "to" in Kyoto means "city" while in Tokyo the "to" means "eastern", i.e. "capital city" vs "eastern capital".  

## 30 Oct 2024: It is 1 year between summer solstices, but 18.6 years between major lunistices 

In the northern hemisphere, the summer solstice is when the sun appears to rise from the northernmost point on the horizon, leading to the longest day. Between the winter and summer solstices the location of sunrise appears to move northward, and between the summer and winter solstices the location moves southward, but around the solstice the location appears stationary, hence "solstice" from the Latin sol ("sun") and sistere ("to stand still"). The equivalent for the moon is called the lunar standstill, or lunistice, when the moon appears to rise from the northernmost or southernmost point on the horizon, before changing direction. Lunar standstills happen twice every lunar month, in a similar way to the solstices happening twice every year. However, while the location of sunrise on the summer solstice is the same each year, the location of the moonrise on the lunar standstill varies each lunar month. In the northern hemisphere, the northernmost point of moonrise is reached every 18.6 years, and this is called the major lunar standstill, with the southernmost point of moonrise the minor lunar standstill. There is a major lunar standstill in 2024, with the last one being in 2006. It seems likely that megalithic cultures were aware of lunar standstills, and possibly also major and minor lunar standstills. There is a theory that the station stones in Stonehenge mark the major lunar standstills. Source: Stonehenge Visitor Information Centre.

## 6 Nov 2024: An ochlocracy is mob rule

Or in more formal terms, an oppressive majoritarian form of government controlled by the common people through the intimidation of more legitimate authorities. Typically led by a demagogue, aka a "rabble rouser". Although if led by billionaires, would it be a plutocracy rather than an ochlocracy? Or a combination of plutocracy and ochlocracy - perhaps I can coin the term plutochlocracy.

## 15 Dec 2024: The maximum allowed height for buildings in central London is 309.6m AOD

The height limitation is due to the Civil Aviation Authority's aviation safeguarding policy for central London, so the buildings do not "adversely affect the operation of London's airports". Source: [Appendix 2 – draft policies on Tall Buildings and Protected Views](https://democracy.cityoflondon.gov.uk/documents/s99270/Appendix%25202%2520-%2520Tall%2520Building%2520and%2520Protected%2520Views.pdf). AOD is Above Ordnance Datum. Ordnance Datum is the vertical datum used by the Ordnance Survey as the basis for deriving altitudes on maps for Great Britain, currently using the mean sea level recorded at the Newlyn Tidal Observatory between 1915 and 1921.

## 25 Dec 2024: Snowflakes fall at a speed of around 3mph on a calm day

And given the average snow cloud base is around 3 miles above ground, that means it takes a snowflake around 1 hour to fall to the ground on a calm day.

