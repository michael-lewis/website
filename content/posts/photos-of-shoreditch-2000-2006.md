---
title: "Photos of Shoreditch, 2000-2006"
date: "2023-06-17"
lastmod: "2023-06-17"
tags: ["burnt out cars", "shoreditch"]
keywords: ["burnt out cars", "shoreditch"]
description: "This post contains some photos I took in the Shoreditch area between 2000 and 2006."
categories: ["old photos"]
featured: false
draft: false
---


## The area

### Moving to London in 1998

Moving to such a big city was a fairly big deal, given I had spent my early childhood years in a remote valley on a Scottish island, and later childhood years in a small town on the Scottish mainland. Indeed, when I had been looking for my first job after university in 1995, I had looked for one pretty much anywhere in the world apart from London (influenced in part by all of the terrible things my mother had told me about London over the years[^note1]).

But by 1998 I wanted to give it a try. After accepting a job offer in London, I started looking for somewhere to live in the city. I decided to narrow the search down to either a nice area, or somewhere close to work. For various reasons, primarily cost and the high level of competition for accommodation in nice areas, I ended up somewhere close to work. It was an area called Shoreditch.

### Shoreditch at the end of the 20th century

Although Shoreditch has become synonymous with both gentrification and hipsterfication now, it was, to use the estate agent euphemism, very much an "up and coming area" in 1998.

My first night there was Sat 7 Nov 1998. Early in the evening, I started hearing loud bangs like gunshots outside. I started to wonder if all those things my mother had said were true, and worry that perhaps I had made a terrible mistake. I realised afterwards that the bangs were not gunshots but Bonfire Night fireworks.

But the temporary relief didn't last long. In the subsequent months, everyone I knew in the area was burgled at least once, including myself[^note2]. One of my neighbours put some nice planters by their door to try and brighten up the street, but they had to take them away after the local drug dealers kept on using them to hide small packs of heroin. And pretty much everyone who came to visit by car had their car vandalised, sometimes within minutes.


## The camera

For the first year or two living in London I hardly took any photos at all, because all I had was an old film camera and it was very expensive getting film developed (see some further comments on that topic in my [photo of my computer setup from 1993](/posts/a-photo-of-my-computer-setup-from-1993/) post).

However, on 8 June 2000, I splashed out a whopping £599.98 for one of those new fangled digital cameras (a Sony Cybershot DSC-S70). I still didn't take a huge number of photos at first, because memory cards and hard drive storage weren't cheap at the time, hence there aren't that many to share. But hopefully it is an interesting glimpse of an area just as everything was beginning to change, and of an era just before everything became photographed to death. I've added recent photos in some cases to help illustrate the changes.

Although taken with an early digital camera, looking at these photos now, they almost seem crisper and more true-to-life than some of the ones I've been taking with my supposedly state-of-the-art camera-phone recently (the camera-phone photos sometimes have a slightly uncanny feel to them, especially when zoomed in, perhaps as a result of all the software processing performed on them).

Note also that this was before cameras had GPS, so the locations are manually approximated.


## The photos

### Last train to Shoreditch station

These photos are of the last train on the last Sunday the station was open (Sun 4 Jun 2006). I did actually take the last train on the last day (Fri 9 Jun 2006), but it was part of my regular commute from work (which was in Canary Wharf by that time) so was nothing unusual for me, and I didn't think to take my camera. I wish I did because the whole platform was jammed with excitable trainspotters.

![Shoreditch station entrance, 4 Jun 2006](/images/posts/shoreditch/shoreditchstation20060604.jpg)
*[Shoreditch station entrance](https://www.openstreetmap.org/?mlat=51.52259&mlon=-0.07091#map=19/51.52259/-0.07091&layers=N), 4 Jun 2006*

![Shoreditch station line, 4 Jun 2006](/images/posts/shoreditch/shoreditchline20060604.jpg)
*Shoreditch station line, 4 Jun 2006*

![Shoreditch station platform, 4 Jun 2006](/images/posts/shoreditch/shoreditchplatform20060604.jpg)
*Shoreditch station platform, 4 Jun 2006*

### Pedley Street arch

After Shoreditch station permanantly closed, I walked through this arch and the footbridge beyond it each day to get to the next station. You can see the building on stilts in a couple of the station photos above.

The footbridge was pretty disgusting, to put it politely. You had to be very careful where you put your feet, so you didn't step on human faeces or slip on used condoms or get stabbed by discarded syringes, and it was best if you held your breath because the stench of human filth genuinely made you want to vomit.

One day in 2006 a film crew came along, to use the footbridge as a filming location. It turned out to be for a film called The Children of Men. According to the wikipedia entry "the set locations were dressed to make them appear even more run-down", but in this case they actually cleaned it up and replaced it with simulated graffiti and more sanitised pretend filth like scrunched up newspapers and the like. A useful reminder that what you see in films is not real.

![Pedley Street arch, 11 Jun 2000](/images/posts/shoreditch/pedleystreetarch20000611.jpg)
*[Pedley Street arch](https://www.openstreetmap.org/?mlat=51.52276&mlon=-0.06820#map=19/51.52300/-0.06822&layers=N), 11 Jun 2000*

Here's what it looks like at the time of writing. It seems like they've demolished quite a bit, and of course it is now covered in grafitti.

![Pedley Street arch, 14 Jun 2023](/images/posts/shoreditch/pedleystreetarch20230614.jpg)
*Pedley Street arch, 14 Jun 2023*

### The City skyline

The flat roofed building towards the bottom left of the photo below was the first flat in London in which I lived. You can see the Broadgate buildings in which I worked at the time as well in the distance. It was 12 minutes walk to work door-to-door, and 15 minutes to work chair-to-chair (depending on how long I had to wait for the lifts at work).

Adjacent to the flat are some crumbling down buildings, with a yard inbetween. The yard is the space with various overgrown plants visible in between the two buildings with missing roof tiles. I think the buildings had been damaged in the Second World War, and the yard had been used as a rag-and-bone man's scrap yard for most of the time since then. It is now "luxury apartments". I heard that the property developer who built the "luxury apartments" hadn't been able to trace the site's original owner so ended up buying it from the rag-and-bone man who obtained ownership via "adverse possession" (aka "squatter's rights").

The skyline looks very empty in this photo, with Tower 42 the only tall building in the area.

![View of The City, 17 Jun 2000](/images/posts/shoreditch/viewofthecity20000617.jpg)
*View of The City, 17 Jun 2000*

Twenty-three years later, and you have (roughly from left to right) The Shard, The Gherkin, 22 Bishopsgate, Heron Tower, The Cheesegrater, Broadgate Tower, and The Stage. 

![View of The City, 14 Jun 2000](/images/posts/shoreditch/viewofthecity20230614.jpg)
*View of The City, 14 Jun 2023*

### Crumbling buildings

This is another run-down building next to the rag-and-bone man's scrap yard on Cheshire Street. I came home one evening to find it in use as the set for a Second World War bomb site, which is ironic given it probably was actually a Second World War bomb site.

![Cheshire Street, 9 Feb 2002](/images/posts/shoreditch/cheshirestreet20020209.jpg)
*[Cheshire Street](https://www.openstreetmap.org/?mlat=51.52356&mlon=-0.07055#map=19/51.52363/-0.07026), 9 Feb 2002*

It is now more "luxury apartments":

![Cheshire Street, 14 Jun 2023](/images/posts/shoreditch/cheshirestreet20230614.jpg)
*Cheshire Street, 14 Jun 2023*


The funny thing about these buildings on Sclater Street is that they look much the same around 20 years later.

![Sclater Street, 26 Jun 2000](/images/posts/shoreditch/sclaterstreet20000626.jpg)
*[Sclater Street](https://www.openstreetmap.org/?mlat=51.52358&mlon=-0.07318#map=19/51.52358/-0.07318&layers=N), 26 Jun 2000*

Although the lower levels are now covered with "street art". As I was taking the new photo, I had to wait for a large group of people on one of the many paid "street art" walking tours to file past.

![Sclater Street, 14 Jun 2023](/images/posts/shoreditch/sclaterstreet20230614.jpg)
*Sclater Street, 14 Jun 2023*



### Smashed up and burnt out cars

For a year or two, it seemed like most weekends I would wake to see plumes of black smoke rising from some nearby street, where cars had been set alight overnight. It was kind-of like a war was going on, and I had unwittingly moved to the front-line.

![Burnt out car, off Pedley Street, 16 Apr 2001](/images/posts/shoreditch/burntoutcar20010416.jpg)
*[Between Pedley Street and Cheshire Street](https://www.openstreetmap.org/?mlat=51.52305&mlon=-0.06830#map=19/51.52305/-0.06830&layers=N), 16 Apr 2001*

![Smashed up taxi, off Rochelle Street, 16 Apr 2001](/images/posts/shoreditch/smasheduptaxi20010416.jpg)
*[Sonning House, Rochelle Street](https://www.openstreetmap.org/?mlat=51.52591&mlon=-0.07360#map=19/51.52591/-0.07360&layers=N), 16 Apr 2001*

![Burnt out cars, between Pedley Street and Cheshire Street, 7 May 2001](/images/posts/shoreditch/burntoutcars20010507.jpg)
*[Between Pedley Street and Cheshire Street](https://www.openstreetmap.org/?mlat=51.52305&mlon=-0.06830#map=19/51.52305/-0.06830&layers=N), 7 May 2001*

![Burnt out car, Rhoda Street, 17 Jun 2001](/images/posts/shoreditch/burntoutcar1_20010617.jpg)
*[Rhoda Street](https://www.openstreetmap.org/?mlat=51.52537&mlon=-0.07239#map=19/51.52537/-0.07239&layers=N), 17 Jun 2001*

![Burnt out car, Rochelle Street, 17 Jun 2001](/images/posts/shoreditch/burntoutcar2_20010617.jpg)
*[Rochelle Street](https://www.openstreetmap.org/?mlat=51.52595&mlon=-0.07409#map=19/51.52595/-0.07409&layers=N), 17 Jun 2001*

![Burnt out cars, Rochelle Street, 1 Jan 2002](/images/posts/shoreditch/burntoutcars20020101.jpg)
*[Rochelle Street](https://www.openstreetmap.org/?mlat=51.52595&mlon=-0.07409#map=19/51.52595/-0.07409&layers=N), 1 Jan 2002*

![Burnt out car, Calvert Avenue, 1 Jan 2002](/images/posts/shoreditch/burntoutcar20020101.jpg)
*[Calvert Avenue](https://www.openstreetmap.org/?mlat=51.52615&mlon=-0.07562#map=19/51.52615/-0.07562&layers=N), 1 Jan 2002*

![Smashed up car, Rochelle Street, 7 Apr 2002](/images/posts/shoreditch/smashedupcar20020407.jpg)
*[Rochelle Street](https://www.openstreetmap.org/?mlat=51.52598&mlon=-0.07429#map=19/51.52598/-0.07429&layers=N), 17 Apr 2002*

I know it is popular to be critical of gentrification, about how it tears local communities apart and leads to reduced diversity and causes displacement and so on, but at least it means people don't have to worry as much about getting their car set on fire.



[^note1]: My mum had never actually lived in London, and had only been to the incredibly busy, noisy and smelly touristy areas, so in retrospect probably wasn't the most authoritative guide to London.

[^note2]: In my case I walked into my living room one August bank holiday weekend to find a stranger scrambling out of my window. Particularly surprising since I was on the top (3rd) floor.



