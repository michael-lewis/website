---
title: "A photo of my computer setup from 1993"
date: "2021-01-20"
lastmod: "2021-01-20"
tags: ["retro", "computer", "Atari ST"]
keywords: ["retro", "computer", "Atari ST"]
description: ""
categories: ["old photos"]
featured: false
draft: false
---


## Introduction

Photos are pervasive nowadays. My children have had cameras in their faces from the moment they were born. Literally. And they struggle to understand how their parents could go for a year or more without a single photo having been taken of them.

In some ways the profusion of photos is a positive thing. It is always a delight to see how much pleasure many of the elderly get from simply looking through old photos, so in future there should be a much richer source of material to refresh memories and provide solace in old age. Assuming of course that all the photos can be preserved for decades, which is a big assumption, given cloud services shut down, local storage can suffer [bit rot](https://en.wikipedia.org/wiki/Data_degradation), file formats can go obsolete, content can be accidentally deleted, etc.

But in some ways the abundance of photos is a little sad. Photos were expensive, because you had to buy film and get the film developed. As such they were usually reserved for the special occasions in ones life, such as birthdays and holidays. So the photos themselves became special, like a catalogue of all the happy times.

There are very few photos of the every-day scenes that comprised the majority of one's life. That's why it was such a pleasant surprise to find a photo of my old computer setup while clearing the old family home last year:

![A photo of my computer setup from 1993](/images/posts/computersetup/computersetup-mid1993.jpg)


## Dating the photo

There were no notes with the photo, and nothing written on the back, so I don't know who took it or when. I did have a film camera myself, but only ever got one or two rolls of film processed over the space of several years, so I don't think it was from that (in fact I still have the camera with a roll of partly used film inside - could be interesting to get it developed one day). I assume it was taken by one of my parents.

The photo was taken in the room where I stayed during my 2nd, 3rd and 4th years of my first degree at university, so that narrows the time it was taken down to 1991-1994. I can also see the Roland JX-1 synth I got in February 1992, rather than the Yamaha SY85 synth I got in February 1994, so that narrows it down further. Looking more closely, I can also see the Target Audio TR50 speaker stands I got in April 1993. Plus, of course, there's my old computer, an Atari ST, which I sold in September 1993.

So that means the picture was taken between April 1993 and September 1993.


## My first computer

A lot of friends got their first computers at primary school in the early 1980s, when Sinclair Spectrums or Amstrad CPCs or the like were popular (no-one I knew was rich enough for a BBC Micro, or if they were they kept it quiet), but I'd missed out on all that because they were still quite expensive[^note1].

It was 1987 by the time I'd earned enough money with various part-time jobs to buy my first proper computer. It was an Atari 520STFM. I went for an Atari ST rather than an Amiga because the ST was significantly cheaper at the time, and the "power without the price" advertising tag-line appealed. As an aside, all the hardware in the photo was bought with my own money, largely from my part-time supermarket job. I even bought the hi-fi in the photo as an 18th birthday present to myself.

I later upgraded the Atari ST's  memory to 1Mb and put in a double-sided disk-drive to make it equivalent to a 1040STFM. I started out using the TV in the photo as my monitor (it was about that time that I started needing glasses, which may or may not be co-incidence). I later got the lovely Atari SM124 high resolution (640 x 400) monochrome monitor, also in the picture. I made my own switch-box to switch between the two without unplugging (if I recall, the monitor used a 13-pin DIN, and pin 4 was the mono detect, so I just soldered a switch on pin 4, and enclosed in a switch-box from Tandy).

The photo also shows my LC24-10 24 pin dot matrix printer. I was very proud of that, because it was 24 pin rather than 9 pin, giving clearer printouts. You could even make rudimentary colour prints with a special ribbon if I recall. Pretty much all of my university essays were printed out on that printer, at a time when most tutors were struggling to read hand-written essays.

For programming, I started with GFA Basic, and later purchased STOS. I programmed a lot of different things in my spare time, largely music and audio related, e.g. a system which "learned" your favourite chord progressions (when gets a mention in [wav-nn: Generating wav files with a neural network](/posts/wav-nn-generating-wav-files-with-a-neural-network/)), plus various tools for generating and editing sound samples (I had the ST Replay sound sampler).


## The end of an era, and the dawn of a new one

I was quite sad to leave the Atari ST world after 6 years, but by mid 1993 it was clear the writing was on the wall for the ST series. All the major magazines were giving away the major software packages as free gifts, and there was no major new software development happening for the platform. I was tempted by the Atari Falcon, but while waiting for the rumoured £100 price drop, they actually increased the price by £100. Furthermore, they used custom memory boards and 2.5" rather than the more standard 3.5" hard drives, so 4Mb RAM and an 80Mb hard drive was similar in price to that of a 486 PC.

So I bought a 486DX33 in September 1993. I remember it had a turbo button which I always found amusing, because pressing it made the computer run more slowly rather than faster as you'd expect (it was for compatibility with old software). And I remember the starfield screen saver - if you moved your head back at the same speed as the stars they briefly appeared to be stationary but three dimensional.

Fortunately I was able to transfer all my ST files to the PC, because the ST could read and write PC format disks. I still have my ST files on my hard drive now. It is funny seeing files with genuine last modified dates in the early 1990s.

Within a year, everything had changed. I had started my MSc in Artificial Intelligence, learned about this new thing called the internet and world wide web, and set up my first web site in December 1994. But that's another story.

Anyway, it is great how many memories one old photo can trigger.



[^note1]: The 16Kb ZX Spectrum was £125 in 1982 according to [https://en.wikipedia.org/wiki/ZX_Spectrum](https://en.wikipedia.org/wiki/ZX_Spectrum), and that is equivalent to £451.15 in 2020 according to the [BoE Inflation calculator](https://www.bankofengland.co.uk/monetary-policy/inflation/inflation-calculator).


