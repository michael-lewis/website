---
title: "Today I Learned 2023"
date: "2024-01-01"
lastmod: "2024-01-01"
tags: ["TIL"]
keywords: ["TIL"]
description: "Random facts I learned in 2023."
categories: ["today i learned"]
featured: false
draft: false
---

This is a followup to [Today I Learned 2021](/posts/today-i-learned-2021/) and [Today I Learned 2022](/posts/today-i-learned-2022/).

## 6 Jan 2023: September (from septem, the Latin for 7) was originally the 7th month

And October (from octo), November (from novem) and December (from decem) were originally the 8th, 9th and 10th months in the ancient Roman calendar which had 10 months. The preceding 6 months were Martius (the first month of the year), Aprilis, Maius, Iunius, Quintilis and Sextilis. January and February were added sometime before 451 BCE. Quintilis was renamed Iulius (July) in 44 BCE after Julius Ceasar's assassination, and Sextilis was renamed Augustus (August) after Caesar Augustus defeated Mark Antony in 8 BCE.

## 10 Jan 2023: Women's clothing used to button on the left for the maids that got them dressed

You tend not to see it so much now, but when I was young there were still girl's blouses and coats that buttoned on the left, and I was always afraid of accidentally getting a charity shop or hand-me-down which buttoned up on the "wrong" side and someone noticing at school. Apparently the practice dates back to the 13th century, when usually right-handed maids typically got ladies dressed.

## 9 Feb 2023: The Andon Cord was a rope that, when pulled, stopped all work on the assembly line

It wasn't just a safety mechanism - workers were required to pull the cord and stop the line whenever they saw a problem with a car (the theory being that fixing it should be easier before it moves further down the line), and the team were required to not only resolve that issue but find out the root cause (to prevent it happening again). Apparently, at the Toyota assembly line where it was introduced, it adversely affected productivity in the short term, but improved quality in the long term, not just of the cars but also of the process. It has now been replaced by the Andon Button.

## 21 Feb 2023: Bacteria don't multiply exponentially (as popularly believed) they grow quadratically

This is because bacterial growth is bound by physical space - on a flat petri dish they can grow in 2 dimensions, so the best they can attain is quadratic growth, i.e. x^2 (with a derivative, i.e. slope / rate of change, of 2 * x). In 3 dimensions the best they can attain is cubic growth, i.e. x^3, unless they are swirled around to create spaces in between but in that case any higher growth rate would not be maintainable.

## 4 Mar 2023: It is thought impolite to put elbows on tables because the tops used to be unfastened

e.g. tables used to be boards on stumps or trestles, and so could be flipped over if you leaned on them.

## 19 Mar 2023: A sprue is a channel through which metal or plastic is poured into a mould

And a "sprue cutter" is a special took for cutting sprues. Not knowing this made it a little tricky to realise that the children's model making instructions I was helping follow had misspelled/mistranslated "sprue cutter" as "spruce cutter" (which in my mind would be much larger and shaped more like an axe or saw). FWIW, once realising what a sprue (and hence a sprue cutter) actually was, I was able to successfully improvise with a wire cutter.

## 27 Mar 2023: Any children from a morganatic marriage have no claim to possessions or title

A morganatic marriage is a marriage between people of unequal social rank, and both the spouse of lower rank and any children have no claim to the possessions or title of the spouse of higher rank. It usually applied to royalty, and was most common in Continental Europe. There isn't really an equivalent in the United Kingdom.

## 3 Apr 2023: If a maze contains just one route and no loops, then it can be constructed in 2 parts

And also can also be solved by the "wall follower" maze solving technique (keeping one hand on one wall). If there are loops and/or multiple routes, then it will need to be constructed in more parts, and will need an algorithm like Trémaux's algorithm (essentially a depth-first search) to solve. Source for the "constructed in just 2 parts": 2:39 of [https://www.youtube.com/watch?v=81ebWToAnvA](https://www.youtube.com/watch?v=81ebWToAnvA). Source for maze solving techniques: [https://en.wikipedia.org/wiki/Maze-solving_algorithm](https://en.wikipedia.org/wiki/Maze-solving_algorithm) .

## 9 Apr 2023: The "pugilistic posture" is the boxer-like posture of burn victims and cremated people

Caused by the way body tissues and muscles shrink in the heat.

## 19 Apr 2023: "Starboard" is on the right for the steering oar and "port" in on the left for docking

The right hand side is where the (mostly right handed) sailors used to have the steering oar, and "port" is the other side which was free for the docking. The terms were used to disambiguate left and right, like "stage right" and "stage left" vs "house left" and "house right" in the theatre, or buccal (cheek-side) and lingual (tongue side) for dentists.

## 25 Apr 2023: The sea level on the Pacific side of the Panama Canal is 20cm higher than the Atlantic

The sea levels don't level out by flow around the Drake Passage because of various factors such as water density, weather and ocean currents. And the locks in the Panama Canal are primarily to lift ships at either end to an artificial lake 26m above sea level, which was created to reduce the amount of excavation work required for the canal.

## 17 Jun 2023: Adjectives usually come in a certain order

The order is: 1. General opinion, 2. Specific opinion, 3. Size, 4. Shape, 5. Age, 6. Colour, 7. Nationality, 8. Material, e.g. "nice handsome young man" rather than "young handsome nice man". Source: [https://learnenglish.britishcouncil.org/grammar/english-grammar-reference/adjective-order](https://learnenglish.britishcouncil.org/grammar/english-grammar-reference/adjective-order).

## 26 Jul 2023: UK "A" roads can be single track lanes, while "B" roads can be dual carriageways

I had always assumed the "A" designation was for wider or better quality roads than "B" roads. I found this one out when I booked my [holiday accommodation on the Isle of Mull](/posts/london-to-the-isle-of-mull-in-a-4-year-old-electric-car/#day-5-fort-william-to-isle-of-mull), planning to use the electric car chargers either 16 miles in one direction or 20 miles in another direction down the A849, not realising that the A849 is a single track road. According to the [Great Britain road numbering scheme](https://en.wikipedia.org/wiki/Great_Britain_road_numbering_scheme) "B roads are numbered distributor roads, which have lower traffic densities than the main trunk roads, or A roads. This classification has nothing to do with the width or quality of the physical road, and B roads can range from dual carriageways to single track roads with passing places".

## 18 Oct 2023: The trowies of folklore may have had some basis in fact

The "trowies" or "trows" of Shetland and Orkney folklore are small dark "huldrefolk" (hidden people) who live in "trowie knowes" (mound dwellings) and only come out at night. This much I knew well - my parents bought the land they built their house on for cheap because the locals said it was a "trowie knowe" (and I used to have frequent nightmares which my dad said might have been caused by the trowies, which didn't exactly help). Anyway, what I learned today was that this piece of folklore may have had some basis in fact. The stories of the trowies began after the Norse invasions, and one theory is that the indigenous peoples survived for many generations longer than believed by hiding and only coming out at night to scavenge: "many stories exist in Shetland of these strange people, smaller and darker than the tall, blond Vikings who, having been driven off their land into sea-caves, emerged at night to steal from the new land owners" (quote from Joan Dey referenced at [https://en.wikipedia.org/wiki/Trow_(folklore)](https://en.wikipedia.org/wiki/Trow_(folklore)#CITEREFDey1991)).

## 30 Oct 2023: Some animals appear to have a reverse/backwards knee, but it is an ankle

For example, cats and most birds appear to have a reverse/backwards knee, but it is really an ankle working as a second knee. These are ["digitigrade legs"](https://en.wikipedia.org/wiki/Digitigrade), in contrast to ["plantigrade legs"](https://en.wikipedia.org/wiki/Plantigrade) like those of humans. Digitigrade legs move more quickly and quietly, whereas plantigrade legs have better stability and weight-bearing ability.

## 17 Nov 2023: Sea shanties were originally work songs to help synchronise rhythmical group labour

With the pacing and structure dependent on the activity being performed, e.g. long hauling, short hauling and heaving. They began to fall out of use towards the end of the sailing era. Oh, and my favourite sea shanty, What Shall We Do With The Drunken Sailor, is in E Dorian, which is the same as Eclipse from The Dark Side of the Moon (with other tracks from that album also in Dorian mode).

## 24 Nov 2023: There are blue and red bananas

The banana variety with a bluish skin is called [Blue Java](https://en.wikipedia.org/wiki/Blue_Java_banana), and the one with a reddish-purple skin is called [Red Dacca](https://en.wikipedia.org/wiki/Red_banana). Given that the blue bananas are supposed to taste like vanilla ice cream or vanilla custard, and bananas in custard is a great comfort food, I'd really like to try these. Some varieties of red banana are suppose to have a slight raspberry flavour, so keen to try those too, in case they're as good as the raspberry flavoured red kiwifruit I found in a local supermarket once a few years back but have never seen again.

