---
title: "Climbing the three peaks (Snowdon, Scafell Pike and Ben Nevis)"
date: "2023-09-17"
lastmod: "2023-09-17"
tags: ["hiking", "snowdon", "scafell pike", "ben nevis", "scotland"]
keywords: ["hiking", "snowdon", "scafell pike", "ben nevis", "scotland"]
description: "The three peaks in this context are the three highest peaks in Great Britain (Scafell Pike, Snowdon and Ben Nevis). Some people do the national three peaks challenge, which is to climb all three in a short space of time, but I did it over 3 years: Snowden in 2021, Scafell Pike in 2022 and Ben Nevis in 2023."
categories: ["hiking"]
featured: false
draft: false
---


## Introduction 

The "three peaks" in this context are the three highest peaks in Great Britain[^note1]:
- Scafell Pike, England, 978m
- Snowdon (Yr Wyddfa in Welsh), Wales, 1085m
- Ben Nevis (Bheinn Nibheis in Scottish Gaelic), Scotland, 1345m

Some people do the "[national three peaks challenge](https://officialthreepeakschallenge.co.uk/national-3-peaks-how-it-works/)", which is to climb all three in a short space of time (usually 24 hours, sometimes 3 days) but I did it over 3 years: Snowden in 2021, Scafell Pike in 2022 and Ben Nevis in 2023.

The last two I climbed with my oldest child (aged 10 for Scafell Pike and 11 for Ben Nevis), which I think is pretty good (although she was too excited by the [Snowdon Mountain Railway](https://en.wikipedia.org/wiki/Snowdon_Mountain_Railway), as I probably would have been at her age, to climb Snowdon).

Now that I've completed all 3, I thought I'd write-up some notes.

For all 3 I just chose the most popular and easiest routes. I didn't have any GPS tracking apps running to show the route and timings, but using [exiftool](https://exiftool.org/) to extract a GPX file from my photos and overlaying on OpenStreetMap I can get a pretty good indicator (unfortunately other stats like distance and elevation aren't so accurate because I didn't always take photos exactly at the start and end).


## Snowdon, 26 Jul 2021

I took the Llanberis Path, starting from the Snowdon Mountain Railway Llanberis Station, or in my case the large car park opposite. There were plenty of parking spaces when I got there before 09.00, but I think it often gets full later.

![Snowdon route](/images/posts/threepeaks/snowdonroute.jpg)

The big surprise was the 45 minute queue for the summit:

![Queue for the Snowdon summit](/images/posts/threepeaks/snowdonsummitqueue_sm.jpg)

From what I've subsequently read, that isn't too unusual.

Nice far-reaching views while waiting though. Apparently the view between Snowdon and Merrick in Scotland (or more likely between Merrick and Snowdon) is [the longest theoretical line of sight in the British Isles at 232km](http://viewfinderpanoramas.org/panoramas.html#longlinesbrit).

Anyway, here's the summit marker:

![Snowdon summit](/images/posts/threepeaks/snowdonsummit_sm.jpg)

Noticing the sunburn on the person in front reminds me I was lucky with the weather. The next day two people at the summit were [struck by lightning](https://www.bbc.co.uk/news/uk-wales-57999183).

Timings:
- Start: 09.05
- At queue for summit: 11.25
- Summit: 12.10
- Lunch break: 12.45-13.40
- Finish: Approx 15.00


## Scafell Pike, 27 Aug 2022

Of the 3, Scafell Pike was the most difficult to get to. Both Snowdon and Ben Nevis are near major towns (Llanberis and Fort William respectively), but Scafell Pike is a long way from any accommodation and transport options. The road to Wasdale is also single track, which wasn't too bad when you are part of a long convoy going in the same direction, but could be problematic for things like the shuttle bus which allegedly took people from a park and ride style car park to the starting point (although we never saw any running at the time). Fortunately we were able to get one of the last parking spaces in the relatively small National Trust car park. I think there's a field nearby which they use as an overflow car park too.

From the Wasdale Car Park we headed up alongside Lingmell Gill, rather than the Corridor Route.

![Scafell Pike route](/images/posts/threepeaks/scafellpikeroute.jpg)

It took around 2hr 20mins of walking to reach the summit, pretty much the same time it took to reach the queue for the Snowdon summit. Much more space at the top of Scafell Pike though, so no queue as such:

![Scafell from the summit cairn](/images/posts/threepeaks/scafellpikesummit_sm.jpg)

And some nice clouds on the way down:

![Nice clouds above Wast Water](/images/posts/threepeaks/scafellpikeniceclouds_sm.jpg)

Timings:
- Start: 10.21
- Summit: 12.42
- Lunch break: 12.42-13.21
- Finish: 14.58


## Ben Nevis, 24 Jul 2023

For Ben Nevis, we took The Mountain Track, starting from the Ben Nevis Visitor Centre.

![Ben Nevis route](/images/posts/threepeaks/bennevisroute.jpg)

Definitely a noticeably longer climb than Snowdon or Scafell Pike, taking nearly 4hrs to reach the summit as opposed to 2hr 20mins, but I guess it is nearly 50% higher than Scafell Pike.

What was particularly surprising was the change in temperature. I'd checked the [mountain weather information service](https://www.mwis.org.uk/forecasts/scottish/west-highlands) and had seen that it would probably be 5-9C at the top of Ben Nevis (which is normal for summer), so I'd packed some extra clothes. But I didn't fully appreciate what 5-9C actually meant in practice, especially when it was around 20C when I set off. I'd put on some extra layers on the way up, but when I was taking photos at the top I noticed my hands had gone purple. I hadn't packed any gloves for the cold, but fortunately had packed gloves in case of midges. Unfortunately my daughter only had one glove, so I lent her one of mine. That was quite a different experience to Snowdon and Scafell Pike, where I started in a t-shirt at the bottom and was still fine in a t-shirt at the top. I guess they're both significantly less high and also further south.

Not much of a queue for the summit photo fortunately:

![Ben Nevis summit](/images/posts/threepeaks/bennevissummit_sm.jpg)

After a packed lunch at the top, me and me and my daughter really enjoyed some big chunks of [tablet](https://en.wikipedia.org/wiki/Tablet_(confectionery)) to help give us the energy for the return journey. I've always quite liked tablet, but only in very small doses, given it is essentially just sugar flavoured with condensed milk and butter. But it really felt like we'd discovered the true purpose for tablet.

The summit is in cloud 70% of the time[^note2], but we got better views when we came out of the clouds:

![Cloud level at Ben Nevis](/images/posts/threepeaks/bennevisview_sm.jpg)

Timings:
- Start: 09.05
- Halfway point (Red Burn): 11.12
- Summit: 12.57
- Lunch break: 12.57-13.30
- Halfway point (Red Burn): 15.14
- Finish: 17.05


## Conclusion

So there you have it. I know experienced mountaineers won't be impressed, but I'm actually really pleased with my achievement - I don't have opportunities for regular training (my nearest hill is the 98m high Parliament Hill), I don't have a great head for heights (I have the rational [fear of falling](https://en.wikipedia.org/wiki/Fear_of_falling), not to be confused with the irrational [fear of heights](https://en.wikipedia.org/wiki/Acrophobia) given I'm fine if I feel safe), not to mention that I'm not quite as young as I once was.

Also, my youngest presented me with certificates after each climb, which I now have on the wall by my desk:

![Three peaks certificates](/images/posts/threepeaks/certificates_sm.jpg)



[^note1]: Great Britain is 3 countries (ordered by population; England, Scotland and Wales), not to be confused with the United Kingdom which is 4 countries (ordered by population; England, Scotland and Wales and Northern Ireland).

[^note2]: According to [https://www.highlifehighland.com/bennevis/fascinating-facts/](https://www.highlifehighland.com/bennevis/fascinating-facts/)
 
