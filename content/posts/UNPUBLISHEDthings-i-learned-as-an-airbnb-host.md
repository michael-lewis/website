---
title: "Things I learned as an Airbnb host 2021-2022"
date: "2024-10-22"
lastmod: "2024-10-22"
tags: ["letting", "airbnb", "shoreditch"]
keywords: ["letting", "airbnb", "shoreditch"]
description: "This post summarises the things I learned while I was an Airbnb host, from Nov 2021 to Apr 2022 and Nov 2022 to Dec 2022."
categories: ["personal finance"]
featured: false
draft: true
---


## Introduction

This post summarises the things I learned while I was an Airbnb host between Nov 2021 and Dec 2022. I know there is an active debate about the impact of short lets on the residential letting market, but I was just a temporary host while my old flat in [Shoreditch](/posts/photos-of-shoreditch-2000-2006/) was empty and listed for sale[^note1].

It has taken until now to publish the post because I wanted to be completely sure I wasn't going to have to relist it on Airbnb. I know many things have changed in the short let space since 2022, but I think many points will still be relevant and useful to some other potential new hosts.


## The Airbnb listing

![Airbnb listing](/images/posts/airbnb/listing.jpg)

It was first listed from Nov 2021 to Apr 2022, during which time I had 16 bookings. It was then relisted from Nov 2022 to Dec 2022, during which time I had 8 bookings.


## Main things I learned

### It takes quite a bit of time and effort to do it properly

The big lesson, which I suspected at the start, is that it takes a fair bit of time and effort to be a host. If you live nearby that can work fine, but the problem I had was that I lived nearly 1 hour away.

I did engage a company to do both the cleaning and the laundry, which helped. Ideally I'd have visited before the cleaning/laundry, so I could see how each guest left the place, and after the cleaning/laundry, to ensure everything was ready for the next guest. But given that would have involved around 4 hours of travelling for each guest, I settled on just going after the cleaning/laundry. That meant that I wasn't able to give such accurate guest reviews, and when things went missing (more below) I couldn't be sure if it was the guests or the cleaners.

Plus when guests needed assistance, e.g. locking themselves out or requesting additional facilities, that would be another 2 hour round trip, often with little notice. For the 16 bookings for the period Nov 2021 to Apr 2022 I ended up making 31 two hour round trips.

### Increasing the price doesn't reduce bookings, it just reduces your value for money scores

One of the first questions new hosts have is how to price their property. Consensus on the hosting forums is that the prices Airbnb suggest are way too low. In my case I looked at similar listing in the area (bearing in mind that my place had 50% more floor space than most other 2 beds in the vicinity), and started out lowish while I built up experience and feedback.

Given London has a 90 day annual limit for short stays, and that I didn't want to use all my 90 days up at the start of the year, I began to increase my prices, expecting the number of bookings to decrease. But curiously, however much I increased my prices, I still remained pretty much fully booked. The only change was that I started getting some 4* scores for Value for Money instead of 5* scores.

### Those "I got rich quick off Airbnb without owning any property" schemes use "Airbnb arbitrage"

After a month or so of hosting, I got an Airbnb alert on my listing page saying "Start hosting longer stays. We've seen a recent increase in guests looking for weekly and monthly stays close to home". I thought an increase in people wanting to stay close to their homes (rather than their places of work) sounded suspicious, but I clicked Review anyway to get more details. Airbnb recommended a 63% discount for a month long stay, although (as per the comment above on pricing) the Airbnb pricing suggestions are usually pretty wacky. Basic economics suggests that if demand is increasing but supply is not increasing then the price should increase too, so why on earth would Airbnb suggest I significantly reduce the price? All that said, I did briefly set up a reasonable monthly discount.

Within a few hours I'd received a booking request for a 2 month period, from an established "Superhost" who had dozens of properties listed (including several which looked like duplicates, possibly relating to London's 90-day rule, and all with 5 star reviews within short time windows). Now there were lots of red flags, like the "Superhost" wanting to book on behalf of a single "corporate client" but not allowing that "corporate client" to book directly, and wanting to take the discussions offline (i.e. meet in person), etc. So I did a bit of research.

I quickly found out about something called "airbnb arbitrage". If you search the internet for that phrase, or something like "make money from airbnb without owning property", you will find a lot of sites about it. The gist is that the arbitrageur takes a longer let at a greatly reduced price, and then sublets at significantly higher prices, e.g. if they let for a month at Airbnb's suggested 63% monthly discount and then fully sublet at the normal price then they could triple their investment without having to actually own any property.

In my case I know my mortgage terms specifically prohibit me from allowing my tenants/guests to sublet, but even if it didn't I wouldn't be comfortable hosting a potentially large number of people I couldn't vet, not to mention disliking the dishonesty of surreptitious subletting. Furthermore, the subletter would have no incentive to screen guests, because any damage guests do wouldn't be to the subletter's property, and they could guarantee every guest a 5\* review irrespective of any issues they might cause.

I had thought that Airbnb might turn a blind eye to this practice, given subletting would mean they'd get two lots of fees for the same property at the same time, but to be fair the account that originally contacted me does appear to be suspended now. It does make me suspicious of the "Superhosts" with dozens of short-lived properties listed though.

### Once you lose your 5\* status you'll never realistically get it back again

I remember once reading an online discussion about online rating systems and their potential flaws. One of the comments I remember was that not giving a host a 5\* rating on Airbnb was incredibly bad.

I was doing fine at first, with my first 8 ratings being 5\*. Then my 9th was 3\* (long story, but short summary is that I don't think it was a reasonable reason). Having the occasional guest with unreasonable expectations does sound not uncommon.

The overall score is worked out with the following formula:

| Star rating | No of reviews | Total (rating \* reviews) | Average (total / reviews) |
| ----------- | ------------- | ------------------------- | ------------------------- |
| 5           | 8             | 40                  |                      |
| 4           | 0             | 0                   |                      |
| 3           | 1             | 3                   |                      |
| 2           | 0             | 0                   |                      |
| 1           | 0             | 0                   |                      |
| 0           | 0             | 0                   |                      |
| Totals      | 9             | 43                  | 4.78                 |

So eight 5* reviews and one 3* review gets an average of 4.78. That means, in order to get my overall 5* rating back, i.e. an average of 4.99 or higher, I would need:

| Star rating | No of reviews | Total (rating \* reviews) | Average (total / reviews) |
| ----------- | ------------- | ------------------------- | ------------------------- |
| 5           | 199           | 995                 |                      |
| 4           | 0             | 0                   |                      |
| 3           | 1             | 3                   |                      |
| 2           | 0             | 0                   |                      |
| 1           | 0             | 0                   |                      |
| 0           | 0             | 0                   |                      |
| Totals      | 200           | 998                 | 4.99                 |

i.e. to have the next 191 consecutive reviews all 5*. That, of course, isn't ever likely to happen, even if it was to be a long term project and there wasn't the 90 day London limit. Maybe it needs something like the [truncated mean](https://en.wikipedia.org/wiki/Truncated_mean) to make the score robust to a single outlier judge.

Fortunately the next reviews were all good, so I did just manange to achive the 4.8 average required for Superhost status while my last guest was staying. Unfortunately, I almost immediately lost Superhost status because, while the last booking gave it 5\* ratings in all categories and a glowing review, they decided to give it a 4\* overall for some reason (maybe a finger slip?)!

### It appears quite lucrative on the surface, but there are a lot of additional costs

Estate agents estimated that it would achieve around £615 per week (£2,665 per month) as a long let. On Airbnb I got between £3,298.82 and £4,849.88 per month[^note2]. That was without trying to optimise earnings, e.g. I set 1 night preparation time before and after each listing, blocked the calendar for the odd days when I wouldn't be available, etc. If I had optimised I suspect I could have got nearly double the income from the short lets compared to long lets.

However, there are a lot of additional costs for short lets that aren't incurred on long lets, for example:
- £375 to £900 per month energy bills (these were around double during the [global energy crisis](https://en.wikipedia.org/wiki/2021%E2%80%932022_global_energy_crisis)).
- £165 to £275 per month excess cleaning costs (I charged guests £45 for cleaning, but total cost to me was over £99.48 each time).
- £185 per month Council Tax.
- £25 per month water bill.
- £23 per month broadband.
- £23 per month for the key storage and management solution.
- Travelling costs (all those 2 hour trips don't come at zero cost).
- All the toiletries, coffee and biscuits and so on which I left out for the guests, cleaning products for the cleaners, etc.

I reckoned that the additional costs more than cancelled out the additional gains. In my case I still think it was worth doing though, for the additional flexibility (a long let could have delayed a potential sale significantly, or reduced the price dramatically given that very few people would want to buy a property with a sitting tenant).


## Additional points

### The Airbnb party worry still hasn't completely gone away

Before listing on Airbnb I was particularly concerned about the party risk. It was partly because my flat was an unusually large one in Shoreditch, which is a popular area for nightlife. It was also partly because we'd already had a bad experience with Airbnb in the block. A neighbour had listed their flat on Airbnb a few years earlier and had a guest host a large party. Not just a party, but one with professional DJs and a bouncer checking the guestlist outside the block (residents had to prove they lived there to get into their homes). Needless to say it was very loud so no-one who lived nearby got any sleep that night. There was also apparently a lot of damage to the flat afterwards.

Most of the time I got a reasonable degree of comfort from looking at previous reviews of the guests before I accepted a booking. But there was one guest who booked for 20th December with a new profile and so no reviews. I decided to give them a chance, especially since their photo suggested they weren't from the typical demographic. Usually after I accepted a booking there'd be a bit of communication, but in this case there was none. This I put ito down to the user being new and unfamiliar with the normal booking protocol. It however got to the day before and they'd still not responded to any messages. Then out of the blue I got another booking from someone saying:

> "My son had told me about a social gathering that would be occurring on the 20th of this month and he showed me an image of this exact apartment. ... The hosts have also distributed the address around. As a mother I would like to respect your home and hopefully my son and I do not get involved in this".

Long story short, Airbnb do seem to have experience dealing with such cases, and I got the booking cancelled, plus alerted the neighbours to the risk of trouble if people showed up trying to get in.

On the Airbnb host forum afterwards, there were other hosts who claimed there are still people who try to make money hosting parties via Airbnb.

### Give people the benefit of the doubt, because there might be things you don't know about

Just because someone behaves in a certain way doesn't mean they always behave in that way, and there might be something going on in their lives that temporarily affects their behaviour. This is something I was already aware of, but did get a useful reminder of as a host. A guest started giving very brief messages, and not acknowledging or saying thanks to responses to previous requests, so I was starting to think about marking them down for communication, but before submitting my review I found out that she'd come over from the US for her daughter's wedding but her daughter's fiancé caught Covid just before the wedding so the whole thing was postponed.

### It is difficult to run environmentally friendly holiday lets

I did try to make the property environmentally friendly. The listing advertised that all the heating and water was from 100% renewable energy (that is what my energy supplier claimed). The problem I found was that many people did go wild with the energy usage, and some of that was just waste, e.g. leaving all the heaters on full blast with the windows open after check-out.

I also opted for larger multi-use containers of toiletries like shampoo and shower gel, rather than those small single use plastic ones you get in hotels. On the whole that worked okay, but there were several times these did go missing, and (because I rarely visited both before and after guests) I didn't know for sure it was the guests.

For the Nespresso coffee pods, I also got a fairly expensive tin of compostable pods from a local shop, where you could buy low-packaging refills. Again the tin had to be replaced serveral times, and I don't know if guests took it as a souvenir, or if the cleaners found the tin empty and binned it.

### It is possible for 4 guests to use 12 rolls of toilet paper in 2 nights

Just to make it clear, I didn't normally count how many toilet rolls each guest used, but I found out in this case by chance. I normally took some spare rolls with me whenever I visited by public transport, but it just so happened that before this guest I came in the car and took a brand new 16 pack, and after they'd left I couldn't help but notice that there were just 4 rolls left from the 16 pack. I suppose it could theoretically have been the cleaners.

### London's 90 day limit seems pretty fixed and removes the incentive to invest in the properties

Given the popularity of the property, I did investigate what would be required to extend beyond 90 days. For the local council where the property is, it would [require planning permission to change the property from "Class C3" (a residential dwelling) to "Class C1" (hotels, boarding and guest houses)](https://www.towerhamlets.gov.uk/lgnl/planning_and_building_control/Short_term_lets.aspx). I checked on the planning applications portal, and every single application had been rejected, so there was pretty much zero chance.

I also got some useful feedback about things that would have made it better for guests. Some of the changes would have required non-trivial investment, e.g. putting in a new water heating system with individually pressurised showers. Now I wouldn't mind making such improvements if I thought there would be a reasonable chance of recouping my investment, but with the 90 day limit there is no incentive for making such improvements.

One of my previous long term tenants had run a shop in the area, and said that pretty much all the business came from tourists, so tourists do seem to play an important part in the local economy, in which case it seems odd not to encourage investment in places for them to stay.


## Conclusion

So I've learned a fair bit from the experience. Some things I was expecting, but some things were surprising, like the "Airbnb arbitrage" risk and the rating idiosyncrasies. Overall it wasn't a bad experience though, and I may consider it again at some future point, if I wasn't living in an area clamping down on short lets. It would be much better living nearby, and having an incentive to make improvements through being able to do it for more than 90 days a year. In fact I might even enjoy it - meeting lots of interesting people, having a good excuse to try out local restaurants and attractions to recommend (or otherwise) to guests, and (at the risk of sounding glib) helping people form some hopefully happy memories (my children still talk about some of the places they've stayed at on holidays from years ago).


[^note1]: I started trying to sell it during the pandemic in 2021 and had a sale fall through during the [September 2022 mini-budget economic crisis](https://en.wikipedia.org/wiki/September_2022_United_Kingdom_mini-budget).

[^note2]: £3298.82 Nov 2021, £1216.59 Dec 2021 (part month), £4849.88 Feb 2022, £3759.60 Mar 2022, £1133.66 Apr 2022 (part month).

