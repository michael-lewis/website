---
title: "Extracting multichannel audio files from DVDs and SACDs"
date: "2022-12-11"
lastmod: "2022-12-11"
tags: ["multichannel audio", "sacd", "dts", "ac3", "blu-ray"]
keywords: ["multichannel audio", "sacd", "dts", "ac3", "blu-ray"]
description: "This post contains information on preparing media files for playback on my media server, including multichannel audio on DVD and SACD."
categories: ["media server"]
featured: true
draft: false
---



## Introduction

This post contains information on how I've prepared my media files, including multichannel audio from DVD and SACD, for playback on my media server.

The media server is running on a Raspberry Pi with [LibreELEC/Kodi](https://libreelec.tv/), connected to a Denon received via HDMI (with AC3 and DTS passthrough configured), in turn connected to B&W speakers. Further information on the media server setup is in my previous post [Building a Raspberry Pi media server for multichannel audio](/posts/building-a-raspberry-pi-media-server-for-multichannel-audio/).

I only actually have 2 multi-channel SACDs[^note1], and a handful of DVDs with multichannel remixes, plus I don't get chance to listen to them much, so I've probably spent a bit too long on this, but hey.


## Extracting multichannel music from a DVD for lossless playback

The process in summary is:
- Extract the raw AC3 or DTS data without transcoding, converting or whatever, for purity.
- Insert that data into a new mp4 container, again with no re-encoding. The container is simply required for tagging, given raw .ac3 or .dts files can't be tagged and Kodi uses tags for identifying music on import.

When played and passed through HDMI to the receiver, the receiver will receive identical data to that from the DVD, and handle all the decoding and sending to the speakers etc. exactly as it would if you had put in the DVD.

### DVDs that have one audio stream per title

This can be scripted via vlc. You need to know the title and chapter numbers (see instructions at [Extract audio](https://wiki.videolan.org/VLC_HowTo/Extract_audio/) to find these). The example below is extracting title 1 chapter 1 and title 2 chapter 1 into separate files. It assumes the mount point at /media/iso exists (if not create with `sudo mkdir /media/iso`).
```
sudo mount -o loop,ro -t iso9660 disc.iso /media/iso
cvlc --no-sout-video dvdsimple:///media/iso#1:1-1:1 :sout='#std{access=file,mux=raw,dst=./title1.ac3}' vlc://quit
cvlc --no-sout-video dvdsimple:///media/iso#2:1-2:1 :sout='#std{access=file,mux=raw,dst=./title2.ac3}' vlc://quit
sudo umount /media/iso
```
The ac3 files are then wrapped in an mp4 container and tagged accordingly:
```
ffmpeg -i title1.ac3 -map 0 -acodec copy -metadata title="Title 1" -metadata artist="Artist" -metadata album="Album" title1.mp4
```

### DVDs that have more than one audio stream per title

I couldn't find a way to fully script it, so I use Handbrake: select Title, click Audio, click Track List, edit track, select Source Track (e.g. DTS 5.1, AC3 5.1 or LPCM 2.0), and change Encoder to the appropriate Passthru (e.g. if the Source Track is DTS select DTS Passthru). This creates an m4v file, which includes video. To strip out the video, and wrap in a new mp4 container (note the -vn to remove video and -map 0 to remove everything else e.g. subtitles):
```
ffmpeg -i title1.m4v -map 0 -vn -acodec copy -metadata title="Title 1" -metadata artist="Artist" -metadata album="Album" title1.mp4

```

### Configuring Kodi to import mp4 files as music

Note that Kodi doesn't import mp4 files as music by default (see [Supported Audio Formats](https://kodi.wiki/view/Music_tagging#Supported_Audio_Formats)). To import mp4 files, create the following advancedsettings.xml (see [Advancedsettings.xml](https://kodi.wiki/view/Advancedsettings.xml)) in `/storage/.kodi/userdata/` on the LibreELEC server (see [Userdata](https://kodi.wiki/view/Userdata#Location)):
```
<advancedsettings version="1.0">
  <musicextensions>
    <add>.mp4</add>
  </musicextensions>
</advancedsettings>
```

## Ripping SACDs for multichannel audio playback

### Extracting the DSF files from SACDs

The first step is to get access to the raw SACD data. For a long time this simply wasn't possible, then it was only possible via early Playstation devices, but now it is possible via a number of other Blu-ray players, fortunately including the player I got in 2015 (a Sony BDPS7200). Full details of the process are at [https://hifihaven.org/index.php?threads/rip-sacd-with-a-blu-ray-player.3652/](https://hifihaven.org/index.php?threads/rip-sacd-with-a-blu-ray-player.3652/).

#### Configure the Blu-ray player

The settings for my player were:
- Audio Settings - DSD Output Mode "Off" (was "On").
- BD/DVD Viewing Settings - BD Internet Connection "Do Not Allow" (already set).
- Music Settings - Super Audio CD Playback Layer "SACD" (already set).
- System Settings - Quick Start Mode to "On" (was "Off").
- Network Settings - set "Wired" (already set) and note IP address.

#### Create USB stick with the SACD extraction software for the Blu-ray player

1. Create a bootable USB with a DOS format Master Boot Record (MBR). Unfortunately all my USB sticks are Linux format now, now so I had to use gparted: (i) create a new partition table type 'msdos', (ii) create a 'primary fat32' partition, and (iii) set the boot flag.
2. Load the software. My Sony BDPS7200 requires the Sony ARMv7 AutoScript sacd\_extract\_6200 version: (i) download AutoScript.zip from Dropbox link labelled "Sony BDP-S6200/BX620/S7200 (refer to post #994 at [https://hifihaven.org/index.php?threads/rip-sacd-with-a-blu-ray-player.3652/post-105564](https://hifihaven.org/index.php?threads/rip-sacd-with-a-blu-ray-player.3652/post-105564)), and (ii) extract and copy to a folder called AutoScript on the USB.

#### Setup the SACD client software

1. Download sacd_extract 0.3.9.3-107 Linux and Download sacd\_extract GUI Linux from [https://www.videohelp.com/software/sacd-extract](https://www.videohelp.com/software/sacd-extract).
2. Run the sacd\_extract GUI (`java -jar SACDExtractGUI.jar`), and configure the location of sacd\_extract.

#### Insert the disc

Switch off the Blu-ray at the wall (the on/off at the front wasn't sufficient), plug the USB stick into the USB slot at the back of the Blu-ray player, and switch on at the wall. After a few moments the tray opens, at which point you insert the disc, and after a few more moments the tray closes.

#### Run the client software

1. Enter the IP address of the Blu-ray player and ping to check connectivity.
2. Select ISO+DSF, Stereo and Multi-Channel, configure the ISO and DSF output directories, and click Run. The ISO isn't needed here, but just saved for completeness.

It took around 45 mins for each disc, and required around 11Gb of storage.

### Convert the DSF files to multichannel FLAC

Unfortunately, unlike AC3 and DTS, it doesn't look like DSD can be passed through HDMI on Linux at the moment, at least not in Android as per the feature request at [https://issuetracker.google.com/issues/73956275?pli=1](https://issuetracker.google.com/issues/73956275?pli=1). So I've had to take the slightly non-purist approach of converting the original DSD (in the DSF) to multichannel FLAC.

Using ffmpeg preserves the channels, i.e. converting the multichannel DSF creates a multichannel FLAC with no special configuration. Unfortunately, the ffmpeg default, i.e. `ffmpeg -i inputfile.dsf -c:a flac outputfile.flac`, came out at a way-over-the-top 24-bit/352.8kHz, so I used a still-probably-slightly-over-the-top 24-bit/88.2kHz instead:
```
ffmpeg -i inputfile.dsf -ar 88200 -f flac outputfile.flac
```

Keeping the original ISO and DSF files does however mean that the next physical player upgrade doesn't necessarily have to support SACD.



[^note1]: Google is refusing to index this page although not saying why, despite it containing potentially useful information written by a human, so I'm removing the names of the two SACDs I have, in case it isn't indexing the page because it thinks it is promoting unauthorised use or something like that (which, to be clear, it isn't).

