---
title: "London to Orkney, and most of the NC500, in an electric car"
date: "2020-09-05"
lastmod: "2023-09-24"
tags: ["electric car", "tesla", "london", "orkney", "scotland", "NC500", "north coast 500"]
keywords: ["electric car", "tesla", "london", "orkney", "scotland", "NC500", "north coast 500"]
description: "A few years back, I was an avid reader of long distance electric vehicle road trip reports. It seemed like the people making them were intrepid pioneers in a new era of transportation. Well here's my slightly belated contribution to the genre. The route was based on a trip I took with my wife in August 2003. We stopped at Loch Ness first, then went up the east coast, decided to do a day trip to Orkney from John O'Groats, then back along the north and down the west coast."
categories: ["electric car"]
featured: true
draft: false
---

## Introduction

A few years back, I was an avid reader of long distance electric vehicle road trip reports. It seemed like the people making them were intrepid pioneers in a new era of transportation. Well here's my slightly belated contribution to the genre.

## Summary

### Route

![Route and summary](/images/roadtrip-routeandsummary.jpg)

### Charging stops

![Charging summary](/images/roadtrip-chargingsummary.jpg)
![Charging detail](/images/roadtrip-chargingdetail.jpg)

Screens courtesy of the excellent [TeslaMate](https://docs.teslamate.org/).

### Cost

I was still using free supercharger miles from the referral scheme[^note1], so the supercharger stops were all free. Most of the [ChargePlace Scotland](https://chargeplacescotland.org/) (CPS) chargers were also still free at the time, although the last 3 CPS charges I made weren't (they were £6.72 for 23.181kWh, £4.18 for 18.922kWh, and £6.82 for 30.718kWh). So the total cost of the electricity for 1857 miles was £17.72 (i.e. just under 1p per mile). If I had been paying for all the electricity, the cost of the trip would have been £118.08 (assuming 492kWh used and £0.24 per kWh), in contrast to £269.80 for petrol (assuming 1857 miles, 38.8mpg and £1.24 per litre). Not to mention that a fossil fuel car would have belched nearly 1 tonne of carbon and other pollutants into the relatively clean Scottish air[^note2].

## Planning

The route was based on a trip I took with my wife in August 2003. We stopped at Loch Ness first, then went up the east coast, decided to do a [day trip to Orkney from John O'Groats](https://www.jogferry.co.uk/Tours/Maxi.aspx), then back along the north and down the west coast. This was long before that part of the route was branded the [North Coast 500](https://www.visitscotland.com/see-do/tours/driving-road-trips/north-coast-500/) (NC500).

In those days we just improvised as we went along, and knocked on the doors of B&Bs with Vacancies signs when it got to that time of day. Needless to say, a bit more planning was required this time around, given a number of new constraints, e.g. the new found popularity of the route, the impact of the pandemic (with many B&Bs closed and many requiring a minimum of 2-3 nights given the extra cleaning required), and that this time around we needed places which could accommodate not just us but also two young children.

Going in an electric car didn't really add much complexity to the planning. Interestingly, one of the tips that someone (who was unaware we were going in an electric car) gave us before we went was "fill up whenever you can because it could be a long way to the next petrol station", so the planning wasn't necessarily that much different from that for a fossil fuel based vehicle. Anyway, I have to admit that the planning is part of the fun for me.

### The route

The main electric car specific constraint I used in terms of the route was: *No more than 200 miles between charging stops, and charging stops coinciding with lunch, dinner etc.*

Although the stated range is 310 miles, to protect the battery it is recommended not to charge beyond 90% or let charge fall below 10%, leaving around 250 usable miles. Driving a car heavily laden with all the things 2 young children seem to need to keep themselves occupied for 2 weeks, and periodically enjoying the phenomenal electric car acceleration, you can easily lose another 20%, taking you to around 200 miles.

### The accommodation

The main electric car specific constraint I used in terms of accommodation was: *Book accommodation near a charger (not necessarily with a charger), so that we could start each day on a 90% charge.*

There didn't seem to be any reliable way of searching for B&Bs providing electric car charging, so the simple workaround was to look for B&Bs on the route with [Tesla destination chargers](https://www.tesla.com/en_GB/destination-charging) or near chargers on the [ChargePlace Scotland Live Map](https://chargeplacescotland.org/live-map/). I also like to try to book B&Bs which have put together their own website, because I like small independent websites (maybe I'll add independent B&Bs with websites to my search engine one day). Anyway, I managed to book a B&B in Orkney with a destination charger, and a B&B in Drumnadrochit in Loch Ness pretty much next to the CPS charger there.

Unfortunately the north and west sections of the NC500 route were a little more tricky in terms of accommodation, but we were able to book places in towns which had CPS chargers, although both towns were a little further down the route than we'd have liked.

## Road trip report

The trip began on Sunday 23 August 2020.

### Day 1: London to the Scottish Borders

There's something about the first 200 miles or so of the drive up from London that I never particularly enjoy. Maybe it is all the relentless traffic from the big cities of London, then Birmingham, then Manchester and Liverpool. Maybe it is the seemingly endless roadworks for the Smart Motorway upgrades (allegedly due for completion in 2022). Maybe it's because the scenery seems to consist of little more than roadside verges and Amazon warehouses.

It is 207 miles from our place in London to the supercharger at Charnock Richard, just south of Preston, so it's always a relief to get there. Afterwards the traffic always seems to improve, the roads have less roadworks, and the scenery gets better. It's a shame the [Motorail](https://en.wikipedia.org/wiki/Motorail_(British_Rail)) doesn't still run between London and Scotland.

Second charging stop was Gretna Green. That was one of the first times I've found a supercharger location with all the stalls occupied, although Gretna only has 4 stalls. I guess the clue should have been the Tesla I saw on the Ionity charger on the way in. It was just a 10 min wait though, so not a big issue. Not sure if it kept the battery preconditioned for supercharging during that time.

I did a final charge overnight in Peebles where we stayed.

### Day 2: Scottish Borders to Loch Ness

First major landmark on this stretch was the Forth Road bridge. One of the children was asleep so missed it.

Then on to Aviemore, for the last stop on the supercharger network for over a week. Didn't really need to stop there, but thought it would be a good spot for lunch. Had haggis lasagne - wasn't sure about it at first, but soon got to really like it, and the children loved it, so I'm going to try and make some at home (fortunately my local Sainsbury's stocks haggis all year round, not just for Burns Night).

And finally, on to the B&B in Drumnadrochit, [The Glen](https://www.lochness-theglen.com/), pretty much opposite the CPS charger. B&B was good so no issues in recommending it.

The Loch Ness stop was primarily for the children. They'd probably seen more references to the Loch Ness monster on the children's TV programs that they watch than any other aspects of Scotland.

### Day 3: Around Loch Ness

Just needed a quick top-up given the car was fully charged 50 miles earlier at Aviemore. Got a "CCS error" after a few minutes of charging on the CCS charger, which I found was common with the older chargers, but switched to the Type 2 connector and all was fine albeit around a quarter the speed.

Then decided to do a car-free day given the children had spent so much of the past two days in the car. Walked to [Urquhart Castle](https://www.historicenvironment.scot/visit-a-place/places/urquhart-castle/), which the children enjoyed.

Then walked to the [Loch Ness Exhibition](https://www.lochness.com/). Thought it was going to be a bit tacky, but it was actually quite interesting. There were several rooms each covering a different aspect of the Loch, from the history of its formation, to the first alleged sightings, the iconic photos now known to be hoaxes, details of the relatively light marine ecosystem (not enough food to support any particularly large creatures), all the sonar scans, and so on. Also had a reference to the [Loch Ness Phenomena Investigation Bureau](https://en.wikipedia.org/wiki/Loch_Ness_Monster#Loch_Ness_Phenomena_Investigation_Bureau_(1962%E2%80%931972)) from the 1960s which I vaguely remember my dad saying he had volunteered for.

All of this was very different from when we last visited. We'd driven from the Fort Augustus end towards Inverness, and stopped at the first museum we saw, which described itself as The Original Loch Ness Museum. It was looking a little run down and neglected, and seemed to simply present every photo and newspaper article as fact, including the [surgeon's photo](https://en.wikipedia.org/wiki/Loch_Ness_Monster#%22Surgeon's_photograph%22_(1934)) which was known to be fake by that time. After leaving The Original Loch Ness Museum, we continued up the road, and within a few 100m came to a much better maintained building which described itself as The Official Loch Ness Museum. Given we'd already spent quite a bit of money at The Original we didn't go into The Official, but it has been a running joke in the family ever since as to whether something is The Original or The Official. Anyway, The Original has now been rebranded Nessieland, which was closed on this visit.

### Day 4: Loch Ness to Orkney, via NC500 east coast route

Had an early-ish start on Wednesday. I think we got on to the NC500 route at Muir of Ord, but the route finder seemed to want to keep suggesting faster routes, so for that leg I had to put destinations in town by town.

Chanced across a couple of not-quite-commissioned CPS chargers on the way - one at Brora and the other at Wick (supplementing Wick's other 2 locations). It is reassuring that the charging network is still being so actively extended, and interesting that they don't show on the map until ready for use (in contrast to the superchargers which can appear as "coming soon" for years).

Stopped at John O'Groats for the obligatory photos, much to the protestation of the oldest child who couldn't fathom why so many people were interested in "just a sign". Still wasn't impressed when we later met some cyclists who had cycled from Lands End to John O'Groats over 12 days (approx 80 miles per day via their route).

Then headed to the ferry. There are two car ferries to Orkney - one operated by [Northlink Ferries](https://www.northlinkferries.co.uk/) from Scrabster to Stromness, and the other by [Pentland Ferries](https://www.pentlandferries.co.uk/) from Gills Bay to St Margarets Hope. The former is supposed to be a more scenic route, via the Old Man of Hoy, but takes 30 mins longer and is more expensive, so we chose the latter.

Was 30 mins early at the ferry terminal, so gave a quick top up in the free CPS bay (the other bay was occupied by a fully charged car).

I have to say I was pretty nervous about the car on the ferry. I remembered to switch off the Tilt Alarm before getting on. However, within 5 minutes of the ferry departing I heard a familiar sounding car alarm from the car deck below, and sure enough I got an alert on my phone. None of the staff complained to my knowledge though. It seems that certain activities turn the Tilt Alarm back on. It was fine for the return journey, and I heard several other car alarms going off then, so staff must be used to it.

Our B&B, [Bankburn House](http://www.bankburnhouse.co.uk/), was just a few mins from the ferry terminal. The host was very welcoming, and keen to help us connect to one of his destination chargers.

![On the destination charger at Bankburn House B&B](/images/roadtrip-day4-orkneydestinationcharging_small.jpg)

He has his own wind turbine that powers the destination charger, and was very keen to talk about Teslas, renewable energy and climate change, which I enjoyed doing too. Apparently there are 4 Teslas on the islands, including his. We didn't see any of the other 3 while we were there, but did hear from other people that there are 4 Teslas on the islands.

### Days 5 & 6: Around Orkney

First trip on Orkney was to the [Brough of Birsay](https://www.historicenvironment.scot/visit-a-place/places/brough-of-birsay/), accessed via a tidal causeway, which we managed to get on and off without issue. It was the most northerly point of our trip.

![Brough of Birsay](/images/roadtrip-day5-1orkneybroughofbirsay_small.jpg)

We went past [Scapa Flow](https://en.wikipedia.org/wiki/Scapa_Flow) where the German fleet was scuttled in 1919 (now the world's primary source of [low-background steel](https://en.wikipedia.org/wiki/Low-background_steel)). Stopped at a beach there. I really like the close clouds that you sometimes see in the Northern Isles - it feels like a blanket over the top of the world.

![Close clouds at Scapa Beach](/images/roadtrip-day5-2orkneyscapabeach_small.jpg)

Weather was very changeable. One local commented that there are "five seasons in a day" (I didn't ask what the 5th was, but assume it was something that didn't quite belong to one of the other 4), and another local remarked "if you don't like the weather, just wait 5 minutes and it'll be different".

Spent the rest of the afternoon in the capital, [Kirkwall](https://www.orkney.com/explore/kirkwall). Evening was a nice dinner at [The Sands Hotel](http://thesandshotel.co.uk/) with very fresh scallops and a local beer on tap, [Scapa Special](https://www.swannaybrewery.com/products/scapa-special), which was just too good (interestingly one of my other all time favourite beers on tap, [Northern Light](https://www.orkneybrewery.co.uk/beer/cask-northern-light), is also from Orkney - there must be something in the water there).

The next day it was off to [Skara Brae](https://www.historicenvironment.scot/visit-a-place/places/skara-brae/). When we'd booked the trip to Orkney it had been closed with no indication as to reopening date, but just before we left they announced that they were reopening on our last day there, and we were fortunate enough to be able to book some of the limited tickets. When we'd visited 17 years earlier, you had been able to walk through the site, which helped visualise how incredibly short Neolithic people must have been, but nowadays you are restricted to walking around it.

After that we came back via the [Ring of Brodgar](https://www.historicenvironment.scot/visit-a-place/places/ring-of-brodgar-stone-circle-and-henge/), built long before Stonehenge and the Pyramids of Giza. Again, 17 years earlier, you had been able to walk right up to the stones, but now, although there wasn't a fence as such, there were signs saying to stick to the path around the site. My oldest was slightly disappointed because apparently I'd spent their "entire life" telling them about these old stones you could actually touch. That was slightly made up for by the [Stones of Stenness](https://www.historicenvironment.scot/visit-a-place/places/stones-of-stenness-circle-and-henge/) which you could still walk up to.

![Stones of Stenness](/images/roadtrip-day6-orkneystonesofstenness_small.jpg)

### Day 7: Orkney to Ullapool, via NC500 north and west coast route

Got up early for the ferry crossing, and left Gills Bay around 09.00. Scenery started getting really nice around Tongue, so we stopped for a morning coffee there.

![Kyle of Tongue](/images/roadtrip-day7-1kyleoftongue_small.jpg)

Coffee was rubbish (I don't mind rubbish coffee if it is free or very cheap, but I don't like paying £2.50 for it). Tried to get a quick extra charge during the coffee break, but it was an old CPS machine and CCS errored after 60 seconds, although I did get 15 miles of range on a Type 2 in the following 20 minutes.

Then on to Durness. That was one of the places I'd have liked to have stayed at overnight, but the B&B options were limited, and the CPS there had been listed as Out of Service for many weeks. Still managed a visit to [Smoo Cave](https://www.visitscotland.com/info/towns-villages/smoo-cave-p245631) which the children loved.

After Durness we saw several convoys of rally cars (some with NC500 branding), a few groups of boy racers in Golf GTIs or whatever, one fleet of expensive sports cars including a Lamborghini and three Ferraris, and 3 other Tesla Model 3s (I did overhear a young boy in Durness say "that's the second Tesla I've seen today" when I parked so I wasn't the only one counting). Saw one of the Golf GTIs upside down in a ditch shortly after Durness (we stopped to check and everyone was okay). Also lots of pretty unpleasant human residue from inconsiderate NC500 folks and/or "wild campers" (we'd come prepared with a trowel and bags to follow the [Scottish Outdoor Access Code](https://www.mountaineering.scot/activities/camping), although even with 2 young children were able to get by just fine without needing it thanks to the great list of [Highland Council public toilets](https://www.highland.gov.uk/info/283/community_life_and_leisure/814/highland_council_public_toilets)). Maybe related, but later on we saw some "No to the NC500" signs - I can imagine the route being popular with some businesses but perhaps not so much with some residents.

The stretch from Unapool to Drumbeg was perhaps the most difficult driving. The 25% gradient was a breeze in an electric car, but I could imagine some fossil fuel based ones struggling. The single track blind summit was just a crazy idea for any car though.

Drumbeg rewarded us with a breathtaking view, fantastic weather, clean public toilets for the kids, and a magnificent stag walking along the road. The only thing that could have made it more perfect would have been if the shop selling Orkney ice cream had been open.

![Drumbeg](/images/roadtrip-day7-2drumbeg_small.jpg)

Had to press on because we had slightly more distance than we'd have liked to cover in a day. But then soon had to stop at Clashnessie for another lovely beach and some fantastic whispy clouds.

![Whispy clouds at Clashnessie](/images/roadtrip-day7-3cloudsatclashnessie_small.jpg)

Finally made it to Ullapool a little later than expected. Hadn't realised the CPS charger nearest the hotel wasn't free, but the second charger in the town was free and wasn't far away. It was a newer one so the CCS worked. Got an average of around 33kW so it was just shy of the fastest CPS speed we got, and so we got from 26% to 93% charge in around 1.5 hours.

We managed to find the same restaurant that we'd eaten at 17 years earlier, [The Argyll Hotel](https://www.theargyllullapool.com/). That was where my wife had eaten her first haggis. She'd sensibly ordered the chicken balmoral, so if she didn't like the haggis she could still eat the chicken. Food was good then and still good now. Only issue was that it was 40 mins after ordering before the food arrived, by which time I had to move the car, meaning I came back to less than hot food. That was probably the biggest disruption caused by charging on the whole trip, but you could argue that the issue was more the time the food took rather than the charging time.

### Day 8: Ullapool to Fort William, via NC500 west coast route

Started out with pouring rain at Ullapool. Wasn't worried though, given that weather there can be changeable, and even if it didn't change we'd had some great spots the previous day.

Three of the next 4 CPS on or near our route to the next overnight stay were listed as Out of Service (The Torridon, Skye Bridge, and Glen Shiel). We didn't need to charge before Fort William, but decided to stop for a break at Gairloch and charge anyway. Met another Tesla driver there. It was their first trip in a Tesla, which they'd just hired in Stirling. Their first issue was that the hire place hadn't given them a fully charged car, telling them that they'd been unable to charge it because it was doing a software update. Their second issue was that they were unable to charge because they had no mobile signal and just had the CPS app not the NFC card. And their third issue was that it was an older Model S that didn't support CCS, so they had to use the much slower Type 2. I didn't have mobile signal either but fortunately my wife did, so was able to hotspot them so they could at least begin charging. Three and a half hours it said they were going to have to wait. Not a great first experience with a Tesla. On the other hand, I was able to use my CPS NFC card and get up to 90% charge on the CCS in the few mins it took to take the children to the ice cream shop and the public toilets.

On to Torridon, then to Shieldaig for a packed lunch. Weather had cleared and was lovely by this time.

![Shieldaig](/images/roadtrip-day8-1shieldaig_small.jpg)

Of course, with the water calm enough for reflections, the air was still enough for midges. We had come prepared though, with multiple packs of [Smidge](https://www.smidgeup.com/) and Skin So Soft Original Dry Oil (formerly known as Skin So Soft Woodland Fresh Dry Oil), along with midge nets and hats and gloves. But in this case the spray was enough, and only one bite was had among us. In fact, during the whole trip we probably ended up with fewer bites than we'd have got from the London mosquitoes if we'd stayed at home. I'm starting to believe that if you're prepared for something (good or bad) it is less likely to happen, e.g. if you go out with an umbrella it is less likely to rain or conversely if you forget your sunglasses it is more likely to be sunny.

Shieldaig village was picture postcard perfect. The youngest also had a very memorable event there - three weeks after getting their first wobbly tooth, it finally came out eating an apple.

Soon after reluctantly moving on from Shieldaig, we saw an official looking Caution Red Squirrels Crossing sign. Didn't see any red squirrels crossing that day, although we did see one crossing the road 3 days later (fortunately we didn't run it over). The children had been enjoying spotting different animal crossing signs. In addition to the usual signs warning about horses, deer, elderly people, and the like, we had seen or were to see ones for otters, frogs, ducks, and (my favourite) feral goats. 

Although we didn't see any red squirrels on this stretch, we did see some Highland cows on the road near Kalnakill. And sheep, plenty of sheep. At one point we were driving past a stone wall when one sheep appeared out of nowhere at the top of the wall and jumped down, followed by another, and another. The children said it was just like something out of a Shaun the Sheep episode, expecting an elaborate construction on the other side of the wall to help them over and perhaps even some dummy sheep so the farmer wouldn't notice their absence. This triggered us to read up about sheep - apparently they are more intelligent than most people think.

While the Tesla's road visualisation might be great at showing traffic cones, it wasn't so good at showing sheep. In fact, I don't think it showed any at all. Most of the time the sheep just didn't appear as anything, but we did catch a time when a sheep was detected as a person, and got a photo of this (albeit not the clearest one).

![Sheep showing as a person on the road visualisation](/images/roadtrip-day8-2sheepasaperson_small.jpg)

Someone should tell the AI that in this case "Four legs good, two legs bad."

We then had another break at Applecross, including some nice local ice cream, before heading to the [Bealach na Bà](https://en.wikipedia.org/wiki/Bealach_na_B%C3%A0) mountain pass with its fantastic view of the Isle of Skye from the summit.

![View of the Isle of Skye from Bealach na Bà summit](/images/roadtrip-day8-3bealachnabasummit_small.jpg)

The road was definitely not for the faint hearted though, culminating in what seemed like zig zags down the side of a cliff.

Left the NC500 route at Strathcarron. Had hoped to stop by Skye on the way to Fort William, but it was later than we'd have liked again, so we pressed on, although did manage a quick stop at [Eilean Donan](https://en.wikipedia.org/wiki/Eilean_Donan).

Saw 4 other Teslas on this part of the journey throughout the day. Not sure if any were the same as those we'd seen the previous day.

Finally got to Fort William. In the centre of Fort William there are currently two CPS stations: a fast free one, and a slow paid-for one. I was hoping to use the fast free one. Unfortunately, while en route, the fast free one went Out of Service. I was glad of the extra charge at Gairloch, because it meant charging wasn't urgent. Still wanted a little extra charge for the next day though, in case we decided to go for a day trip somewhere far away, so went to the slow paid-for charger. There was a fully charged Nissan van occupying one of the two spots, but I was able to squeeze into the second spot and do a bit of a paid-for slow charge while we got some dinner. When I went to move the car a couple of hours later, the fully charged Nissan van was still there. I wonder if it was using it as a free overnight car park. Given that it was a tight spot, there was no way for another car to have parked and used the charger with the Nissan van still there. A bit disappointing, and not the first time I've seen CPS charging spots blocked by fully charged cars for long periods.

### Days 9 & 10: Around Fort William

We decided to stay local given the amount of driving we'd done. Went up Ben Nevis the first day, and took a boat cruise the second day. It was nice to finally go up a mountain and get on the sea after seeing so many of both over the past week.

Popped by the free fast CPS to check if it actually was Out of Service. There was a Model 3 owner on the phone there to CPS support. He had low charge and was depending on a good charge for the next leg of his journey. Seems it definitely was Out of Service. Saw the spot for the Tesla supercharger which has been "coming soon" for a very long time. Looking at the [photo of the equipment on site from 4 weeks earlier](https://teslamotorsclub.com/tmc/threads/uk-supercharger-site-news.91118/page-84#post-4899097), it didn't look like they'd made any progress. So I topped up charge again via the slow not-free charger. The van had finally moved, and there was a Model S charging on one spot, making it a bit of a squeeze to get into the other.

![Tight charging spot at Fort William](/images/roadtrip-day9-chargingatfortwilliam_small.jpg)

### Day 11: Fort William to the Scottish Borders

Still some nice countrside at Glen Coe, and skirted the Trossachs, passing through Callander (birthplace of [Helen Duncan](https://en.wikipedia.org/wiki/Helen_Duncan), the last person to be imprisoned under the Witchcraft Act of 1735, in the surprisingly late year of 1944[^note3]). Had planned on visiting Stirling Castle and The Kelpies, but it was raining heavily, and we saw the latter from the car, so just pressed on.

### Day 12: The Scottish Borders to London

Stopped in Peebles to stock up on [Forsyths](https://forsythsofpeebles.com/) steak pies, and Moffat to take the children to the sweet shop where I finally got to buy some Moffat Toffee for the first time ever despite passing through Moffat between 4 and 8 times every year for nearly a decade when I was young[^note4] (spoiler alert: I finally discovered, after all that time, that Moffat Toffee isn't actually toffee).

Had the first supercharge in 9 days at Tebay Southbound. Was hoping to have another leisurely uninterrupted lunch, but had forgotten how fast superchargers are, so lunch was interrupted.

Then the long, slow slog back to London. Largely uneventful journey, apart from a stretch with all but one lane closed due to a car on fire, and another section with all lanes on both sides closed due to a "pedestrian on the road" (she looked deeply unhappy) - very different problems to sheep on the road.

By this stage, the children had started to entertain themselves by trying to spot other Teslas too. Saw 11 in the last 1 mile, which could have been almost as many as we'd seen in the preceding 1856 miles put together.

## Conclusion

So there it was. A fun journey, and not compromised at all by being in an electric car. Indeed, for some of the more difficult stretches such as the steep gradients, being in an electric car was brilliant. The supercharger network was excellent as usual, and the ChargePlace Scotland network pretty good overall too. Just had issues with CCS on the old CPS machines, and saw on the map that quite a few CPS were Out of Service, but on the plus side I did see new ones being installed. Charging more often than was needed did help, e.g. when the Fort William CPS went Out of Service, and it is definitely worth getting the CPS NFC card rather than relying on the CPS mobile app for places where mobile signal may be patchy.

So it all seemed pretty routine. Perhaps the new era of transportation is upon us now.


[^note1]: Update Sept 2023: Looks like they've restarted the referral programme. My current referral link is [https://ts.la/michael93471](https://ts.la/michael93471) for £500 or $500 off.

[^note2]: According to [https://co2.myclimate.org/de/calculate_emissions](https://co2.myclimate.org/de/calculate_emissions).

[^note3]: Although she was prosecuted under section 4 of the Act which covered fraudulent "spiritual" activity.

[^note4]: Not entirely sure why we'd never bought any in all that time. Maybe it was because my grandmother, who we were on our way to/from visiting when we went through Moffat, made her own toffee, and there was a strong resistance within the family to spending money on something that was normally homemade.


