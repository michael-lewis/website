---
title: "Growing a cherry tree on a north-facing balcony 2019-2021"
date: "2021-07-02"
lastmod: "2024-07-07"
tags: ["cherry tree", "balcony", "plants"]
keywords: ["cherry tree", "balcony", "plants"]
description: "My struggle to grow a cherry tree on a north facing balcony, from 2019 to 2021"
categories: ["plants"]
featured: false
draft: false
---


## 2019

### The origin story

I like cherries, and for many years I've cultivated the notion that I'll have "made it" when I can sit in my own garden underneath my very own cherry tree. Unfortunately, the realities of city-living have meant that I've been in flats for the past few decades. But I do now have a balcony (somewhat optimistically described as a "roof terrace" in the estate agent brochure), albeit a north-facing one.

I'm also quite interested in growing things. I find articles like [Fruit Walls: Urban Farming in the 1600s](https://solar.lowtechmagazine.com/2015/12/fruit-walls-urban-farming.html) and [Fruit Trenches: Cultivating Subtropical Plants in Freezing Temperatures](https://www.lowtechmagazine.com/2020/04/fruit-trenches-cultivating-subtropical-plants-in-freezing-temperatures.html) really interesting. I also like the interesection of technology and growing things, e.g. I saw a "Personal Food Computer" from the [Open Agriculture Initiative](https://en.wikipedia.org/wiki/Open_Agriculture_Initiative) at an exhibition in 2019, and considered trying to build one myself, although I couldn't seem to find any groups which had successfully built one, plus I'm not too keen on basil which seemed to be about the only thing people were allegedly growing in it.

There is also a bit of a family history of growing things too. When I lived with my parents in the Shetland Islands, they managed to grow what they described as some of (or perhaps the only) fruit bushes on the islands, noting that the islands are so windswept that there were hardly any trees of any kind at all (although in recent decades that has started to change). I remember my dad saying that when wind hits a wall (or obstacle) there is a period of space behind the wall where the shelter is actually higher than the wall, and that he had designed and built the wall and worked out the optimal distances for the 1st, 2nd and subsequent bushes to maximise their potential size. I think this might be the ["law of the wall"](https://en.wikipedia.org/wiki/Law_of_the_wall) in fluid dynamics.


### 16 March 2019 - The first tree

With all of that in mind, one day I was in the outdoor section of the local Homebase, and decided, in a rare example of impulse buying, to purchase a cherry tree. It was only £15 for a 2-year-old Y-shaped tree that was about my height. This was before I had a car, and the tree was too big for the bus by the time I'd lifted it off the ground, so I lugged it all the way home by myself, its two branches frequently catching in other bushes and trees overhanging the pavement on the way home. I then plonked it on my balcony, sat underneath it, and thought (almost defiantly) "I've made it".

In the days that followed, I did more research on growing cherry trees (usually the research would come before a purchase, but I had, after all, been overcome by a sudden desire to make my dreams come true). Unfortunately, it seemed it might not be that simple. On the plus side, I had bought a variety (Stella) which was self-pollinating, so I wouldn't need a second tree. But on the minus side, the label didn't indicate the rootstock, so I had no idea if it was suitable for a container or not. Nevertheless, I persevered through summer, and kept the tree alive, although none of the small amount of blossom turned into viable fruit that year.

### More about the balcony

At this point I should note that although the balcony is north-facing, it is off the dormer of a converted loft-space, and gets a surprising amount of direct sun over the roof top for much of the day from mid-spring to late-summer. I had also successfully grown other summer fruits there, such as strawberries and blueberries, by that point. And I noticed that the branches of the cherry tree were sufficiently high to be in the direct sun pretty much all day over spring and summer. So I didn't think it was doomed to be a fruitless endeavour, so to speak.

### December 2019 - The new tree

I did, however, start to think that if I was going to invest time and effort into growing a cherry tree properly, I should at least get a tree with a known root-stock, so I'd know it would be suitable for the environment. I did a bit of research online, and settled on another Stella with a "Gisela 5" dwarf rootstock. This would grow to a height of 3m after 10 years if unpruned, although if kept in a container it would have its growth restricted. I found a supplier online, and added it to the Christmas ideas list.

As Christmas approached, it seemed my wife wasn't too keen on having a cherry tree on the balcony for some reason. I did however mention it to my sister, and she was quite taken with the idea, despite it being (at over £50 including delivery) well over the usual budget. So for Christmas 2019 my sister bought me a new cherry tree. She had it delivered to her home address, 120 miles away.

I went to visit her on Sat 28 Dec 2019, and saw the tree for the first time in her garden. It was around the same height as the original tree, but with a more full crown. The original tree, with its Y-shaped crown, may have been for fan-training against a wall, but this looked more like the "open crown" "bush" style (it looked like the "central leader" had been pruned). It definitely looked like a professionally raised tree.


## 2020

### January 2020 - Getting the new tree home

By that time I had my first car (the one I later used to drive from [London to Orkney, and most of the NC500](/posts/london-to-orkney-and-most-of-the-nc500-in-an-electric-car/)). The problem was that I couldn't see how to get the tree safely in the car with one other adult and 2 children. I decided to return a couple of weeks later, on Sun 12 Jan 2020, just by myself. That was 240 miles of driving, for a cherry tree. Things were starting to get serious. I moved the original Y-shaped tree to outside the communal front-door on the south-facing side of the house, where it sort-of lies flush with the wall[^note1].

Here's the first picture of the new tree in-situ:

![Cherry tree, 8 February 2020](/images/posts/cherrytree/cherrytree20200208_sm.jpg)

### February/March 2020 - High winds

There were some pretty windy days in late winter and early spring, and on the top floor it is especially exposed to the elements, so the tree got a bit of a battering. It was much worse in spring when the branches were laden with leaves. It was quite stressful to watch at times, so I tried to avoid looking.

### April 2020 - Holes in leaves

By April I noticed a problem though - it was getting quite a few holes in the leaves:

![Cherry tree leaves developing holes, 30 April 2020](/images/posts/cherrytree/cherrytree20200430_sm.jpg)

The holes were of varying sizes, some small pinholes, and others quite large. At first I thought it might be some form of insect attack, but I couldn't see any signs of insects. Then I thought it might be a disease, but it seems that diseases like ["leaf spot" and "leaf scorch"](https://www.rhs.org.uk/advice/profile?pid=567) are typcially accompanied by patches of discolouration on the leaves, which wasn't present, so I wasn't convinced that was the cause either.

### May 2020 - The war on slugs

I thought I found the problem in May, when I realised there were a lot of slugs on the balcony, and that they only really came out to eat leaves at night when no-one was looking. So I launched a full scale war on slugs. In addition to the (now banned) slug pellets, I tried non-chemical approaches, like the copper strips around the pot, and wool pellets on the soil (which made the tree smell like a farm-yard for the first few days). I even considered biological warfare using nematodes, such as the fabulously named "Nemasys" range. At the end of the day though, I suspect the most effective approach was simply to check all the pots on the balcony every night and pick off the slugs. Some nights I was finding 15-20 slugs, and reckon I must have picked off over 100 in total. Quite where they all came from I'm not sure, given the balcony was probably much too high for them to have slithered all the way up from the gardens below. I assume they came from some shop-bought pots, and multiplied over time.

### June 2020 - Leaf curl

I'm not sure if it was related to the holes in leaves and/or the slug problem, but by June all the leaves were starting to curl up:

![Cherry tree leaves curling, 14 June 2020](/images/posts/cherrytree/cherrytree20200614_sm.jpg)

### 21 June 2020 - The first crop

Despite all these challenges, 12 cherries survived to ripen:

![The first 12 cherries, 20 June 2020](/images/posts/cherrytree/cherrytree20200620_sm.jpg)

We picked the first one on 21 June 2020 (Father's Day), and 3 others on 24 June. They were a little bitter to be honest. I read a few weeks later about some people being [poisoned by courgettes](https://www.bbc.co.uk/news/uk-england-suffolk-53402497), with the posion being a chemical called [cucurbitacin](https://en.wikipedia.org/wiki/Cucurbitacin) which plants produce as a defence against attack, so I have a (completely unproven) theory that the slight bitterness might have been as a result of the tree releasing chemicals to fend off the (presumed) slug attack.

### 27 June 2020 - Bird attack

On 27 June, the day we were planning to pick the remaining 8 cherries, disaster struck! About 1 hour before we were planning to pick them, and with two children sitting on the sofa less than 2m away, most of the cherries mysteriously disappeared, and the remainder developed holes that looked suspiciously like bird beak marks.

### July 2020 - Leaf drop

By mid July, the leaves were turning brown and starting to drop:

![Cherry tree leaves turning brown and starting to drop, 19 Jul 2020](/images/posts/cherrytree/cherrytree20200719_sm.jpg)

This is apparently not uncommon after the leaf curl. Strangely, in autumn some buds appeared at the end of the branches, and some leaves even opened before winter, although they didn't survive long.

### Other crops

Although my cherry growing wasn't especially successful in 2020, I did become one of those [lockdown balcony gardeners](https://www.bbc.co.uk/news/uk-52844039) you read about, successfully growing strawberries, blueberries, raspberries, potatoes, peas and carrots on my small balcony. And I still had space for a couple of miniature paddling pools for the children over summer. I might not have much space, but I try to make the most of what I have.


## 2021

Over winter, I moved the tree closer to the wall to try to give it more protection from the wind, and maybe even get a little heat from the house:

![Cherry tree in the snow, 9 Feb 2021](/images/posts/cherrytree/cherrytree20210209_sm.jpg)

### May 2021 - Holes in leaves, and leaves only at the end of branches

When the new leaves appeared in spring 2021, they were mostly on the new growth at the ends of the branches, where the leaves had briefly reappeared in autumn. Also, before long, there were the holes in the leaves once more:

![Cherry tree with holes in the leaves, and leaves only at the end of branches, 17 May 2021](/images/posts/cherrytree/cherrytree20210517_sm.jpg)

This time, however, there were hardly any signs of slugs or snails on the balcony, so I'm starting to wonder if that was the cause of the holes after all. I even posted to [Gardeners' World](https://forum.gardenersworld.com/discussion/1054409/holes-in-cherry-tree-leaves-and-leaves-only-at-the-end-of-branches) to see if anyone had any thoughts.

One idea I had was that perhaps it was the stress of the wind causing the holes. Orchards are often in walled gardens[^note2], and I suspect that is not just to prevent the wind from blowing the fruit off or to make it more difficult for people to steal the fruit. Known problems wind can cause to trees include dehydration. As an aside, if I were ever to try to grow cherry trees in a [controlled-environment](https://en.wikipedia.org/wiki/Controlled-environment_agriculture), it seems they might actually need a little wind, given one of the findings from the [Biosphere 2 project](https://en.wikipedia.org/wiki/Biosphere_2) was that many types of trees grow weak and misshapen without some wind to form [stress wood](https://en.wikipedia.org/wiki/Reaction_wood).

I also wonder if the leaves only at the end of the branches issue was due to the leaf drop and subsequent brief appearance of new leaves in autumn. The new leaves had only opened on the new growth, and they dropped off before winter, leaving the new growth ready for spring. However, the leaf buds which appeared on the old growth didn't fully open, and didn't drop off, potentially preventing new buds from appearing in spring. "Rubbing off" these dead buds may resolve next year.


### June 2021 - Netting the tree

When the fruit started showing signs of starting to ripen in early June, I attempted to net the tree. I was only able to put up 3 poles because there was the balcony door where the 4th pole would go, and I wasn't able to fully close the net on the side next to the balcony door, so it wasn't exactly an impenetrable fortress, but it seemed to be good enough:

![The netted cherry tree, 25 June 2021](/images/posts/cherrytree/cherrytree20210625_sm.jpg)

You can see a reasonable crop of cherries starting to ripen.


### 28 June 2021 - Split cherries

On 28 June, just as the first cherries were about ready to pick, disaster struck! Overnight, heavy rain fell, and pretty much all the cherries split significantly:

![Split cherries, 28 June 2021](/images/posts/cherrytree/cherrytree20210628_sm.jpg)

Actually, they had started splitting in the preceding days, given it was was a pretty wet June, but after that night it seemed noticeably worse. The cherry clusters which weren't sheltered by leaves seemed particularly badly affected.

If cherries get wet in the final two weeks or so of ripening, some of the water is absorbed through the skin and the cherry swells causing the skin to split. In parts of the US they sometimes use [helicopters to dry cherries](https://www.aneclecticmind.com/2009/06/13/the-life-of-a-cherry-drying-pilot/), but in the UK they typically cover the trees, which is what I may have to do next time.

So after nearly 2.5 years, still no bowl of home-grown cherries. Maybe next year.


## Conclusion

If there's a moral to the story, I guess it is that it can take time and effort to make your dreams come true, and that even then they might not. Or maybe it is that you should have simpler dreams, or something like that. But anyway, one thing I have learned - while gardening is partly about getting close to nature, it is also about constantly battling against nature.


## Update (7 Jul 2024)

For all those reaching this page as a result of internet searches such as "cherry tree leaves turning brown and curling", I'm reasonably sure that the issue, at least in my case, is wind stress rather than pests, given that all the leaves were nice and lush when the tree was in a sheltered south facing patio for one year - see also [Growing a cherry tree on a north-facing balcony 2022-2024](/posts/growing-a-cherry-tree-on-a-north-facing-balcony-2022-2024/#2022).


[^note1]: As of June 2021, it is still there. It hasn't grown, perhaps because it is still in a relatively small pot, and hasn't produced any fruit, but at least the leaves look healthy.

[^note2]: Interestingly, the term "walled garden" has something of a negative connotation in technology, because it refers to closed ecosystems and keeping people out. However, in horticulture it is more positive, being for "horticultural rather than security purposes" (source: [walled garden, wikipedia](https://en.wikipedia.org/wiki/Walled_garden)), e.g. due to the heat absorption properties of walls and the shelter they provide from wind and frost.

