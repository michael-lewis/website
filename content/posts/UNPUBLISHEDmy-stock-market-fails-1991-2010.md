---
title: "My stock market fails 1991-2010"
date: "2024-10-22"
lastmod: "2024-10-22"
tags: ["stock market", "investing"]
keywords: ["stock market", "investing"]
description: "A short summary of the bad stock market investments I made between 1991 and 2010, and the lessons I learned from them."
categories: ["personal finance"]
featured: false
draft: true
---


## Introduction

This is a short summary of the bad stock market investments I made between 1991 and 2010, and the lessons I learned from them.


## My epic stock market fails

### Ferranti, 1 Nov 1991

I bought my first shares when I was still at university, trying to stretch out the little money I had from part/full time work[^note1].

I purchased 4000 Ferranti shares on 1 Nov 1991. Ferranti was a well-known technology company, and a major employer near the part of Scotland in which I lived (it was the first technology company in what would become known as "[Silicon Glen](https://en.wikipedia.org/wiki/Silicon_Glen)"). It was founded in 1885, and highlights from its long history include delivering the first commercially available electronic computer in 1951[^note2], and producing microelectronics for computers such as the Sinclair ZX Spectrum and BBC Micro in the 1980s. In 1987 they even expanded internationally with the purchase of a US-based company called International Signal and Control (ISC).

Unknown to me (this was before the internet), the day before I purchased my shares, i.e. 31 Oct 1991, the ISC founder and 18 others were indicted on charges of a $1.14Billion fraud.

To quote the [wikipedia page](https://en.wikipedia.org/wiki/Ferranti#Collapse):

> "ISC's business primarily consisted of illegal arms sales started at the behest of various US clandestine organizations. On paper the company looked to be extremely profitable on sales of high-priced 'above board' items, but in fact these profits were essentially non-existent. After the sale of the company to Ferranti in 1987 all illegal sales ended immediately, leaving the company with no obvious cash flow, and the merger led to the ultimate collapse of Ferranti".

Not the best introduction to US foreign policy and business ethics for an impressionable young student, to put it politely.

I still have the share certificate[^note3]:

![Ferranti share certificate](/images/posts/stockmarketfails/ferranticertificate_sm.jpg)

### NASDAQ stocks, 15 March 2000

In 1999, a few years after finishing university, I bought a flat in [Shoreditch](/posts/photos-of-shoreditch-2000-2006/). As a result, I was a little stretched financially, so (despite working in the internet space) I hadn't been able to invest in any of the internet stocks that were doing so phenomenally well at the time. However on 15 February 2000 I got my biggest ever bonus[^note4].

It took a few weeks to get my Personal Trading Account set up for USD-denominated transactions, and for the purchases to be approved by my employer's Compliance department, but on 15 March 2000 I made a number of significant purchases. Little did I know that the NASDAQ had reached its peak 5 days earlier, on 10 March 2000[^note5].

By the time I came to sell, some of the stocks were worth so little the broker was unable to sell them (the fees would be higher than their value), so the only option was to donate them to charity. Although the NASDAQ did eventually reach that level again 15 years later, the companies I had originally invested in were long gone.

### Banking stocks, 2008

I kept out of the stock market for a number of years as a result of my experience with the dot-com bubble. However, I gradually began reinvesting, deciding to stick to banks because they were supposed to be safe. Most had fallen in value since the UK's first bank run in 150 years (the [Northern Rock](https://en.wikipedia.org/wiki/Northern_Rock) in Sept 2007), which made the price-to-dividend ratio very attractive (the lesson there is of course that if the price-to-dividend is unusually high it probably means the dividends will be cut significantly). Anyway, I bought several banking stocks in the first half of 2008, just before the start of the [Global financial crisis in September 2008](https://en.wikipedia.org/wiki/Global_financial_crisis_in_September_2008).

### BP, 15 April 2010

Still reeling from losses in the financial crisis, given almost all my investments had been in hitherto "safe" banks, I decided to buy shares in BP, which seemed to be a safe investment in a completely different sector, and provided good dividends.

I purchased 780 BP shares on 15 April 2010. Five days later, on 20 April 2010, the [Deepwater Horizon drilling rig exploded](https://en.wikipedia.org/wiki/Deepwater_Horizon_explosion), marking the start of one of the largest environmental disasters in world history.


## Lessons learned

### Stock market investments can actually go to zero

The standard disclaimer on investment products is "investments can go up and down in value, so you could get back less than you put in". What they don't say is that stock market investments can actually go to zero, so you could get back nothing, as happened to me with Ferranti and many of the NASDAQ stocks. In which case you shouldn't invest what you can’t afford to lose.

### Individual stocks are a gamble

You could make case that I should have predicted the dot-com bubble, and possibly even a case that I could have predicted the global financial crisis. However, I don't think I could have foreseen the ISC scandal that brought down Ferranti, and certainly not the Deepwater Horizon explosion. I know there are plenty of stories about people making a fortune from lucky stock picks, but at the end of the day it is luck rather than skill, and there will be plenty of stories you never hear about the unlucky ones.

### Diversify, e.g. via passively-managed tracker funds

This really follows on from avoiding individual stocks. Although they didn't exist until the late 1990s, Exchange-Traded Funds (ETF) are a good way to diversify nowadays. Personally, I prefer passively-managed (i.e. low management fee) tracker funds, and that seems to be the current consensus in various personal finance forums. Although at this point I feel I should add a disclaimer that I am not a professional adviser and nothing here should be construed as investment advice. In fact if my track record is anything to go by, ETFs might be a bubble about to burst:-)


[^note1]: Working in a supermarket 20-30 hours a week during term time and full-time during holidays.

[^note2]: The [Ferranti Mark 1](https://en.wikipedia.org/wiki/Ferranti_Mark_1) was delivered in 1951, ahead of the UNIVAC I which was delivered in 1952.

[^note3]: The Date of Registration shown on the share certificate is 22 Nov 1991, but that date was 3 weeks after the transaction.
 
[^note4]: My 2000 bonus was equivalent to 26% of my salary, which was the biggest bonus I ever received in percentage terms.
 
[^note5]: The NASDAQ Composite index reached 5,048.62 on 10 March 2000.

