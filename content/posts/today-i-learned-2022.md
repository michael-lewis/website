---
title: "Today I Learned 2022"
date: "2023-01-01"
lastmod: "2023-12-29"
tags: ["TIL"]
keywords: ["TIL"]
description: "Random facts I learned in 2022."
categories: ["today i learned"]
featured: false
draft: false
---

This is a followup to [Today I Learned 2021](/posts/today-i-learned-2021/).

## 30 Jan 2022: The Portuguese Man-of-war (the venomous siphonophore) is named after a warship

A man-of-war is a 16th to 19th century warship. The ones from Portugal are also known as caravels. Portuguese Man-of-wars are so named because they look a little like caravels at full sail. They aren't found in the waters around Portugal.

## 20 Feb 2022: Hershey's chocolate tastes like vomit because of the butryic acid (which is in vomit)

But apparently most Americans associate the taste of [butryic acid](https://en.wikipedia.org/wiki/Butyric_acid) with Hershey's chocolate rather than vomit, because they have eaten Hershey's chocolate more often than they have vomited. I assume there are other compounds in vomit which prevent Americans from thinking vomit tastes like Hershey's chocolate.

## 20 Feb 2022: Lichtenberg figures can also appear on grass

Lichtenberg figures are the fern-like patterns that may appear on the skin of lightning strike victims, and they can also appear on grass, e.g. golf courses, especially on the green if lightning hits the flagstick marking the hole.

## 15 Mar 2022: The word bellwether originates from the bell put on the leader of a flock of sheep

The wether was the castrated lamb that the other sheep would typically follow, and the bell would be put around the neck of the wether so the shepherds could hear the movements of the flock if they were out of sight. Hence bellwether as an indicator of trends.

## 14 Apr 2022: Fiddler crabs shed their shells as they grow

It seems all crabs do. I knew snakes did. Was inspired to find this out when the children noticed what they thought were lots of dead crabs, which were actually just discarded shells.

## 19 Apr 2022: The mortar area around chimney pots is called flaunching

And if it needs repairing the process is called reflaunching.

## 23 Apr 2022: Bumble bees are much better fruit tree pollinators than honey bees

They start earlier in the day than honey bees (e.g. 07.30 vs 12.30), will work at much lower temperatures, and will pollinate 6 flowers in the time a honey bee takes to do 1.

## 23 Apr 2022: Cherries normally grow on 2 year old wood, but can on the base of 1 year old wood

Like raspberries, which are normally "floricane" (fruiting on 2 year old canes), but can sometimes be "primocane" (fruiting towards the end of the season on 1 year old canes). Cherries also naturally want to grow upwards, so have to be trained to grow outwards.

## 24 Apr 2022: Meerkats can close their ears

So sand won't get in during a sand storm.

## 2 May 2022: Some Victorian era taxidermists didn't know what walruses looked like

And because they didn't know that walruses have many loose folds of skin, they tended to overstuff them, as per the bloated walrus at the [Horniman Museum](https://www.horniman.ac.uk/).

## 15 May 2022: A whipping boy was actually a boy that was whipped instead of another boy

Usually in place of a young king or prince, because you were not supposed to whip royalty.

## 29 June 2022: A "lacuna" is a lexical gap, a "logatome" is a nonsense syllable

A "lacuna" is a word that exists in one language but has no direct translation in another, e.g. "kummerspeck" in German, meaning when people gain extra weight from emotional binge eating. A "logatome" is a short pseudoword, i.e. a word that sounds like a real word but isn't, consisting most of the time of just one syllable which has no meaning of its own.

## 19 July 2022: All 13 English university-educated PMs from 1945 to 2022 were educated at Oxford

Five of which were schooled at Eton College, indicating an astonishing lack of diversity, and explaining that disastrous combination of arrogance and incompetence which has caused so much damage to the country. Full list at [https://www.ox.ac.uk/about/oxford-people/british-prime-ministers](https://www.ox.ac.uk/about/oxford-people/british-prime-ministers) and [https://en.wikipedia.org/wiki/List_of_prime_ministers_of_the_United_Kingdom_by_education](https://en.wikipedia.org/wiki/List_of_prime_ministers_of_the_United_Kingdom_by_education). Note that Winston Churchil, James Callaghan and John Major were not university educated, and Gordon Brown was not English (he was Scottish, with a degree from Edinburgh University).

## 19 July 2022: Norman surnames are still "significantly overrepresented" at Oxford and Cambridge

Given nearly 1000 years have passed since the Norman conquest, it suggests the class system is incredibly deeply entrenched in English society. Norman surnames are defined as those who appear as landowners in the Domesday Book. Source: p19, "Surnames and Social Mobility: England 1230-2012", Gregory Clark and Neil Cummins.

## 8 Aug 2022: Trypophobia is "an aversion to the sight of ... clusters of small holes or bumps"

Which may have provided some evolutionary advantage. Source: [https://en.wikipedia.org/wiki/Trypophobia](https://en.wikipedia.org/wiki/Trypophobia).

## 12 Aug 2022: In some countries cats are said to have 9 lives, but in others it is 7 lives

In countries such as the UK, cats are said to have 9 lives, possibly originating from the Ancient Egyptians who believed cats came from the 9 underworlds. In other countries such as Portugal, cats are said to have 7 lives, possibly originating from an old Arab proverb.

## 31 Aug 2022: The phrase "black market" comes from the act of smuggling graphite

In 1650 graphite became more valuable than gold, hence a target for thieves, and handling graphite tended to leave back marks, hence the "black market". Source: [The Derwent Pencil Museum](https://www.derwentart.com/en-gb/c/about/company/derwent-pencil-museum).

## 5 Sep 2022: A0 paper is 841 x 1188 mm to meet 2 requirements

It is the only possible ratio of width to length where folding in half preserves that ratio, and has an area of (almost exactly) 1sqm (it is actually 0.999949sqm but is the nearest whole mm width and length which preserves the ratio). A1 is A0 halved, A2 is A1 halved, A3 is A2 halved, and of course A4 is A3 halved. Knowing this you can calculate that an A0 sheet of 80gsm paper will weigh 80g, and therefore an A4 sheet of 80gsm paper (at 1⁄16 the area of an A0 sheet) will weigh 5g.

## 25 Sep 2022: The longest place name in England with no repeated letters is Bricklehampton

And the second longest is a tie between Buckfastleigh and Buslingthorpe.

## 17 Oct 2022: NASA lost the original moon landing tapes

See [https://en.wikipedia.org/wiki/Apollo_11_missing_tapes](https://en.wikipedia.org/wiki/Apollo_11_missing_tapes). Rather than indicating some kind of conspiracy theory or cover up, this simply illustrates how attitudes to television have changed over time - back then it was rare and ephemeral rather than omnipresent and everlasting. It is hard to imagine now, but many classic television shows from before the era of video recorders, like Children of the Stones or Blake's 7, were only ever broadcast once, so if you missed that one broadcast that was it. I even heard that there were strikes from the actors union (possibly including the strike that led the Douglas Adams penned Doctor Who episode being left unfinished) opposing repeats, because they feared showing repeats would put actors out of work (although I haven't been able to find a reference to confirm this). Many other broadcasts from the era of the moon landings have also been lost probably forever, e.g. [many Doctor Who episodes](https://en.wikipedia.org/wiki/Doctor_Who_missing_episodes). This is a very different era from now, when e.g. 500 hours of video are uploaded to YouTube every minute and "the Internet never forgets".

## 23 Oct 2022: Anemoia is "nostalgia for a time one has never known"

The term was coined in 2012. See [https://en.wiktionary.org/wiki/anemoia](https://en.wiktionary.org/wiki/anemoia).

## 25 Nov 2022: Companies whose names begin with A or a number are less likely to be good

The exact quote is "The average plumbing firm whose name begins with A or a number receives five times more service complaints than other firms", from the paper "'A' Business by Any Other Name: Firm Name Choice as a Signal of Firm Quality", by Ryan C McDevitt. If they have to attract custom by appearing at the top of alphabetical lists then they may not be the best. This also applies to online adverts. I used to search for tradespeople with Google Maps, but then I realised there's a big industry of while-labelled SEO-friendly companies which register 100s or 1000s of names like "\<location\> plumber" or "\<location\> roofer" each with a pin on the map at \<location\>. When you contact them you go to a call centre 100s or 1000s of miles away who know nothing about your area, they outsource the actual work to gig workers, and when they start getting negative reviews that instance just disappears. Reviews are another whole issue with the internet. I have seen many cases where positive reviews from genuine customers have been drowned out by negative reviews from random non-customers venting, plus of course there are people who make a living from writing fake reviews.

## 13 Dec 2022: The first number spelled out in English which has the letter a is one thousand

The first number that contains the letter m is one million, and the first number that contains the letter b is one billion (bazillion is not a real number).

