---
title: "Unfinished projects 2004-2022"
date: "2024-10-22"
lastmod: "2024-10-22"
tags: ["artificial intelligence"]
keywords: ["artificial intelligence"]
description: "Side-projects I have spent time working on, but am unlikely to finish."
categories: ["artificial intelligence"]
featured: false
draft: true
---


## Introduction

I sometimes say that one of the great things about keeping a personal website is that it encourages me to finish side-projects, or at least finish them enough to be able to write about them. That said, there are still some side-projects I have spent time working on but haven't finished, and am unlikely to, for whatever reason. So I thought - why not write about some of these as well?

Note that there are a lot of other ideas that I haven't spent time working on, but I'm not going to discuss those here. Although I will start with a couple of notes on ideas first.

### Ideas are free (and easy)

While ideas are free (and easy), it is the implementation that is important (and difficult). I realised this at the start of the dot-com bubble in the mid-late 1990s, when I was regularly approached by people wanting me to implement their ideas (given I set up my first web page in 1994, and ran a well known software company's website from 1995). I soon began to detect a typical pattern: the "ideas person" would excitedly tell me all about their big idea, why they were such a genius for thinking about it, and how they would do all the "hard work" of attending lunches and parties and what not to promote it, leaving me free to stay in and focus on the "relatively simple" part of implementing it. Now I know that marketing is important, but this was well beyond that. Thanks, but no thanks.

### The big idea that got away

It is worth a quick mention of the big idea that got away though. I was approached towards the end of 1999 by a friend who wanted to implement a web site to allow you to say which schools you went to in order to try to reconnect with your former classmates. In early retellings of this story, I believed this was before either classmates.com in the US or friendsreunited.co.uk in the UK, although looking at timelines now I think classmates.com had been active for a while but work on friendsreunited.co.uk hadn't begun. Anyway, I dismissed the idea, because I didn't think people would want to reveal personal details on the internet (using aliases was a common internet safety principle in the early years). Little did I know that a new generation of people would soon be coming online and falling over themselves to reveal endless amounts of their most intimate personal details to "the network". 


## 2004-2005 - Putting the i into iPod

As mentioned in [Building a Raspberry Pi media server for multichannel audio](/posts/building-a-raspberry-pi-media-server-for-multichannel-audio/), I got my first iPod in 2003, and put several hundred CDs on it. It was amazing, being able to listen to so much music on the go. The idea of being able to listen to this on random play (aka shuffle) was also completely new. However, I did have one niggle with shuffle - sometimes a track would play which I didn't remember, and given I like to know what I'm listening to I'd have to get my iPod out of my pocket and check the screen (and later devices like the iPod shuffle didn't even have a screen).

What I really wanted was a button on the remote control (on the headphone wire at the time) that you could press to hear the name of the artist and title for the currently playing track, injected into the audio stream, using metadata and text to speech, to save you having to get the device out and look at the screen. I envisioned a special "i" button on the remote control, styled like the tourist information "i". I joked with some others at the time that I'd be "putting the i into iPod".

I also wanted to try and patent the idea. I think I might have been influenced by all the technical CVs I'd seen from US-based folks which listed out their patents. Maybe I also harboured a secret dream of selling the patent and getting rich, or something like that. Anyway, to patent it, I thought I had to have a working prototype, which I envisioned as a box I'd plug into the iPod via the Apple connector, potentially also taking headphones from the box too. So I set about building a box as a prototype.

So what happened? Well, the USB development libraries were non-free on Windows, but free on Linux. Unfortunately I was still using a bit of both Windows and Linux at home at the time, so I thought this would be a good excuse to finish the migration to use Linux exclusively at home. I won't go into all the details here, but this involved lots of activities completely unrelated to USB development, like cross-converting my audio files from .ape to .flac. I found out later that this kind of procrastination is a known phenomenon and even has a name - [yak shaving](https://en.wiktionary.org/wiki/yak_shaving). I got as far as learning about the USB spec, Apple's proprietary interface, music metadata, and the [Festival text to speech](https://www.cstr.ed.ac.uk/projects/festival/) system, but never got around to putting it all together.


## 2018-2021 - Dementia clock

Sadly, my mother was diagnosed with dementia in April 2018. (If I'd known what to look for, I could have spotted symptoms a lot earlier.) At first we were able to manage with carers looking after her in her own home, albeit with increasing frequency and duration of visits. But one of her symptoms was getting confused about the time of day and wandering off[^note1], and that was particularly difficult if it happened before carers were due.

There are a number of [dementia clocks](https://shop.alzheimers.org.uk/collections/clocks-and-watches) available, but many of them are black and white LCD displays which seem aimed at people who have trouble with eyesight rather than comprehension. I bought the [day and night clock](https://shop.alzheimers.org.uk/collections/clocks-and-watches/products/day-and-night-clock), but that didn't seem to help.

What I really wanted was a clock which simply told you if it was day or night, ideally using colour to make it clearer. I don't actually know if it would have helped or not to be honest, but I wanted to at least try. The inspiration was the [Groclock sleep trainer clock](https://www.tommeetippee.com/en-gb/product/groclock-sleep-trainer-clock), which I'd had some success with a few years earlier with my children[^note2]. This was blue with a big moon at night:

![Time to sleep](/images/posts/unfinishedprojects/clocknighttime_sm.jpg)

and yellow with a big sun during the day:
 
![Time to be awake](/images/posts/unfinishedprojects/clocksunshine_sm.jpg)


I was going to construct one with something like a [Raspberry Pi Pico](https://www.raspberrypi.com/products/raspberry-pi-zero/) with a battery, along with one of the new generation of e-ink displays which supported more than one colour (I don't think white/black/blue/yellow was available at the time, but there was white/black/red/yellow).

Unfortunately, before I had chance to complete this, my mother's condition had deteriorated to a point where it was no longer likely to help.


## 2021-2022 - Popcorn Wizard: Using artificial intelligence to make better popcorn

When I was just starting out living on my own after leaving home, I was a big fan of cooking in the microwave. It was partly because it seemed quick and efficient and high tech, which appealed to me, but partly because I hadn't really learned to cook any other way. However, as I got older, I learned that some things are better and/or easier cooked in the conventional way. One of these things was popcorn. Those little bags of microwave popcorn were relatively expensive, and never particularly tasty because the coating always stuck to the side of the pack rather than the popcorn. In contrast, making it in a pan really wasn't any more difficult - just add butter and popcorn kernels to a pan and cover until ready - and tastier because you could just drizzle with your favourite coating and shake the pan a bit. Of course, one thing was slightly difficult - deciding when to turn off the heat. Too early and you had too many unpopped kernels, and too late you got a burnt flavour.

So I got to wondering - what if technology could help? That's when I came up with the idea for an app that counted your kernels at the start, then counted the pops during the process, showed you the percentage that had popped, and suggested the time to stop.

The first step was of course to look for alternative solutions. There are specialist physical devices for making popcorn, some of which use "air popping" to allegedly avoid the risk of burning. Although some of these sound like they might solve the problem and are relatively inexpensive, it would be yet another single-use kitchen appliance to stuff into cupboards already filled with rarely used single-use kitchen appliances[^note3]. A search online also revealed that there was already an app with a very similar premise, called [Popcorn Expert](https://ai4popcorn.com/), released in 2018. However, it only counted the pops and didn't count the kernels, and hadn't been updated in a couple of years by the time I looked at it in 2021, so I thought it was fair to do my own version.

The first thing I was going to develop was the popcorn counting. In the real world, people would be measuring the popcorn in bowls, so not all kernels would be visible at once, but in the pan they should be mostly on one layer:

![Popcorn in pan](/images/posts/unfinishedprojects/popcorninpan_sm.jpg)

Unsurprisingly, there were no pre-trained object detection models for popcorn kernels, so I started looking for some models that were not too dissimilar that I could fine tune. This involved the laborious process of counting all the kernels. I didn't get quite as far as preparing labelled images, but did count some:

![568 popcorn kernels](/images/posts/unfinishedprojects/568popcornkernels_sm.jpg)

Unfortunately, object detection is currently better at counting a small number of large objects rather than a large number of small objects. Typical config has the bounding box mininum size as 20% of the image and maximum 95% of image, but for popcorn kernels I'd need something like min 1% and max 10%. Furthermore, increasing the total number of anchor boxes exponentially increases computation costs.

So it turned out not to be quite as easy as I hoped it would be, and the project ground to a halt in 2022. The final nail in the coffin was finding out that the first new version of Popcorn Expert in 3 years was released. I also found out a few years later that a US patent had been filed in 2013 and granted in 2020 for ["detecting the status of popcorn in a microwave"](https://patents.google.com/patent/US20160205973A1/en) (although this detects readiness by decreasing pop frequency rather than an initial estimate of total kernels vs pops detected).

To be honest, it really just was an excuse for brushing up my mobile app and machine learning development skills, which I sort of did.


[^note1]: For example, turning up at the hairdressers shortly after dawn, not really knowing what time it was, but not wanting to be late for the appointment.

[^note2]: Although, after they got used to the clock, we were still often woken at weekends by them bursting into the room, only this time it was to excitedly tell us that "sunshine's out" on the clock.

[^note3]: Such as the cast iron fondue set I got given as a present one year. Now I thought it was great and enjoyed the one time I used it, but I'm not sure if I'll get chance to use it again, since my wife isn't interested, one child really doesn't like open flames, and the other child really doesn't like melted cheese.

