---
title: "Walking the London LOOP"
date: "2024-10-20"
lastmod: "2024-10-20"
tags: ["hiking", "london"]
keywords: ["hiking", "london"]
description: "Walking the 150 mile (242km) London LOOP (London Outer Orbital Path)"
categories: ["hiking"]
featured: true
draft: false
---


## Introduction

The London LOOP is the London Outer Orbital Path, a 150 mile (242km) walking route which encircles the city, like a hiking equivalent of the M25 \(the London Orbital Motorway\). There's more good info on the LOOP on the [TFL](https://tfl.gov.uk/modes/walking/loop-walk) and [Ramblers](https://www.innerlondonramblers.org.uk/ideasforwalks/loop-guides.html) sites.

I split the walk over 9 days between May and September 2024, averaging 15-20 miles (24-32km) per day. These are some notes I took.

![LOOP route](/images/posts/londonloop/looproute.png)


## Day 1: Sections 1-2, Erith to Petts Wood (16.2mi / 26km)

The walk starts at Erith, where there's an information board summarising the first section: 

![Section 1 LOOP marker at Erith](/images/posts/londonloop/s01marker_sm.jpg)

The first part of the walk is along the River Thames, passing a number of recycling centres on the right, with the last stretch of the London LOOP visible across the river:

![View across the Thames from Erith](/images/posts/londonloop/s01thamesfromerith_sm.jpg)

The route then turns into the Crayford Marshes, apparently a popular place for wildlife spotters. I heard a cuckoo[^note1] for the first time in decades, and then met someone who asked if I'd seen the cuckoo (he had, but at least I was able to say I'd heard it).

Section 1 ends at Old Bexley, which seems nice.

Section 2, from Old Bexley to Petts Wood, is surprisingly rural, and passes the Scadbury Moated Manor (the remains of a medieval era manor house with a moat).

Stats:
- Travel time to start: 1h 30m
- Walking time: 6h
- Travel time from end: 1h 30m
- Travel cost: £12.70


## Day 2: Sections 3-4, Petts Wood to Hamsey Green (18.6mi / 30km)

Section 3 starts at Petts Wood. I lost the trail a little in the ground of the High Elms Golf Club, and met some other LOOP walkers who had also got lost there. We found our way out in the end. I thought I'd be seeing other LOOP walkers fairly regularly, but it turns out this was the only confirmed sighting in all the 9 days (although there were a couple of times local people asked if I was walking the LOOP, one of whom said he thought I was because I looked like I "was a man on a mission").

Passed the Wilberforce Oak, under which [William Wilberforce](https://en.wikipedia.org/wiki/William_Wilberforce), William Pitt and William Grenville met in 1787, discussing the abolition of the slave trade (a long process eventually ending with the Slave Trade Act of 1807 and Slavery Abolition Act of 1833).

![The Wilberforce Oak](/images/posts/londonloop/s03wilberforceoak_sm.jpg)

Section 4 starts at West Wickham Common. Pretty early on, it passes the Greenwich Meridian marker in the Coney Hall recreation ground:

![Greenwich Meridian marker in the Coney Hall recreation ground](/images/posts/londonloop/s04meridianmarker_sm.jpg)

I took a slight detour to see the Shirley Windmill. I like the idea that people were using environmentally friendly renewable sources of energy long before the industrial revolution. 

The highpoint of section 4 has to be Addington Hill in Sanderstead Plantation, which is the highest point on the whole LOOP, and no 2 on the [List of highest points in London](https://en.wikipedia.org/wiki/List_of_highest_points_in_London). From there it was possible to see Canary Wharf, along with the Shard and the taller buildings in The City poking out above the hill at Crystal Palace:

![View from Addington Hill](/images/posts/londonloop/s04viewfromaddingtonhill2_sm.jpg)

Stats:
- Travel time to start: 1h 20m
- Walking time: 7h
- Travel time from end: 1h 50m
- Travel cost: £18.15


## Day 3: Sections 5-7, Hamsey Green to Ewell (15.5mi / 25km)

Section 5 starts at Hamsey Green, passes Kenley Airfield, and then into the very appealingly named Happy Valley, which includes the southernmost point of the LOOP. Happy Valley then joins the also nicely named Farthing Down. There were plenty of skylarks singing on Farthing Down, and I love listening to skylarks. There was also a bit of a view from Farthing Down, with a clear view to Canary Wharf and tall buildings in The City (just) visible:

![View from Farthing Hill](/images/posts/londonloop/s05viewfromfarthinghill_sm.jpg)

Section 6 starts at Coulsdon South, and involves walking through Mayfield Lavender fields, but unfortunately I was doing this in May rather than July and August so the lavender wasn't in bloom:

![Mayfield Lavender fields](/images/posts/londonloop/s06mayfieldlavenderfields_sm.jpg)

Shortly after the lavender fields, I saw my second LOOP marker at Oaks Park. According to the board, it was here that Lord Derby and Lord Bunbury tossed a coin to decide who got to name a horse race (Derby won and named it The Derby):

![Section 6 LOOP marker at Oaks Park](/images/posts/londonloop/s06marker_sm.jpg)

Section 6 ended at Banstead Downs[^note2]. Section 7 was one of the shorter sections, finishing at Ewell, which had some nice old buildings.

Stats:
- Travel time to start: 1h 40m
- Walking time: 6h 15m
- Travel time from end: 1h 20m
- Travel cost: £15.60


## Day 4: Sections 8-9, Ewell to Hatton Cross (18mi / 29km)

Section 8 was from Ewell to Kingston upon Thames, which as the name suggests is where the LOOP crosses the Thames. It did feel somewhat symbolic crossing the river for the first (and last) time on the LOOP.

Bushy Park at the start of section 9 had lots of lovely skylarks singing, and plenty of not-too-shy deer:

![Deer in Bushy Park](/images/posts/londonloop/s09deerinbusheypark_sm.jpg)

The last part of section 9 was along the River Crane, approaching Hatton Cross, by Heathrow airport. I know in Richmond and Kew the planes are quite noticeable, so I had assumed they would be even more noticeable closer to Heathrow, but surprisingly they weren't as bad as I expected. I think it might be because the runways are from east to west and I was approaching from the south, and perhaps the hills and the valley provided some shielding too.

Stats:
- Travel time to start: 1h 35m
- Walking time: 6h 48m
- Travel time from end: 1h 45m
- Travel cost: £15.00


## Day 5: Sections 10-12, Hatton Cross to Harefield West (16.7mi / 26.9km)

Secton 10 starts at Hatton Cross, right by Heathrow Airport. The route near the start goes through a street which is close to the landing runway, with planes descending just above people's houses. When I was taking photos of the planes above, I got a bit of a dirty look from someone on the other side of the street (not sure why):

![Plane over houses near Heathrow Airport](/images/posts/londonloop/s10planeoverhouses_sm.jpg)

As with the approach on section 9, it didn't take much walking to leave most of the plane noise behind to the south, although the plane noise was soon replaced by noise from the M4, which the route passed under.

Section 11 is from Hayes & Harlington to Uxbridge. It passed through Stockley Park, where Adobe and Apple used to have their UK headquarters, and then along Horton Road, where Tesla currently has its UK headquarters.

The LOOP reaches its westernmost point shortly before Uxbridge where section 11 ends. There was a long diversion due to work on the Grand Union Canal, so I went through Uxbridge where I passed the Crown & Treaty pub[^note3].

Section 12 starts along the canal, and goes under the A40 just before it becomes the M40 (route of the Oxford to London bus, which I took many times when I lived in Oxfordshire before relocating to London). The route goes past lots of lakes and water parks in the Colne Valley, where there's a massive bridge being constructed over Harefield Lake for the HS2[^note4]:

![HS2 viaduct over Harefield Lake](/images/posts/londonloop/s12hs2viaductoverharefieldlake_sm.jpg)

Stats:
- Travel time to start: 1h 35m
- Walking time: 7h 21m
- Travel time from end: 1h 45m
- Travel cost: £10.25


## Day 6: Sections 13-15, Harefield West to Elstree (19.3mi / 31km)

Shortly after starting section 13 at Harefield West, I passed through a meadow which turned out to be filled with butterflies. As I walked they flew into the air leaving a trail of fluttering butterflies in my wake. It was the sort of magical scene I can imagine appearing in an anime film.

Much of the rest of sections 13 (from Harefield West to Moor Park) and 14 (from Moor Park to Hatch End) were a bit of a blur though to be honest, because in Bishops Wood I managed to walk into a branch which somehow flicked my glasses off and gave my left eye a nasty poke. Fortunately it turned out not to be as bad an injury as I expected, and wasn't so bad by start of section 15 in Hatch End.

Section 15 includes Harrow Weald Common, which is the highest point on the LOOP north of the Thames, and no 7 on the [List of highest points in London](https://en.wikipedia.org/wiki/List_of_highest_points_in_London). The viewpoint looks south towards Harrow on the Hill, but I couldn't recognise any landmarks beyond Harrow itself. Shortly after that is the grounds of Bentley Priory (HQ of Fighter Command in World War II), where the view is more to the south east, and I was able to spot some recognisable buildings such as the Wembley Arch:

![View from Bentley Priory](/images/posts/londonloop/s15viewfrombentleypriory_sm.jpg)

Shortly before the end at Elstree, the route passes under the M1, which I have used a fair few times entering and leaving London on the way north, e.g. to the [Isle of Mull](/posts/london-to-the-isle-of-mull-in-a-4-year-old-electric-car/) or [Orkney](/posts/london-to-orkney-and-most-of-the-nc500-in-an-electric-car/).

I think this is also the day I passed the most golf courses: Sandy Lodge (section 14), Grim's Dyke (section 15) and Elstree (section 15). I always get nervous walking through golf courses[^note5].

Stats:
- Travel time to start: 1h 30m
- Walking time: 6h 35m
- Travel time from end: 0h 35m
- Travel cost: £10.45


## Day 7: Sections 16-17, Elstree to Enfield Lock (19.7mi / 31.7km)

Section 16 starts in Elstree, and goes along the busy Barnet Lane for a bit, before heading into Scratchwood. Unfortunately after Scratchwood you have to walk down the busy A1 for a while to get to an underpass to enable you to walk back up the A1 for a while. I don't like walking alongside busy main roads. There's also a lorry park on the A1 northbound where there had been a lot of flytipping over the fence into a gulley in Scratchwood, which is sad to see, especially in such a nice woodland.

Then there's a stretch shared with the Dollis Valley Greenwalk, which connects with the Capital Ring walking route further south. Shortly after departing from the Dollis Valley Greenwalk, and a little beyond High Barnet, there's a lovely looking village called Hadley Green, which has the 3rd LOOP marker I saw:

![Section 16 LOOP marker at Hadley Green](/images/posts/londonloop/s16marker_sm.jpg)

It also contains the house where [David Livingstone](https://en.wikipedia.org/wiki/David_Livingstone) (of "Dr. Livingstone, I presume?" fame) lived (apparently just for a year):

![David Livingstone's cottage](/images/posts/londonloop/s16livingstonscottage_sm.jpg)

Section 17 starts at Cockfosters. The Trent Country Park is perhaps the longest stretch on the whole LOOP where I didn't see a single person. I think I saw a Spitfire in the sky though. There's also a viewpoint where you can see The Shard and some of the tall buildings in The City:

![View from The Ridgeway](/images/posts/londonloop/s17viewfromtheridgeway_sm.jpg)

Then comes the Forty Hall Country Park, which is the northern most point on the LOOP. It is also the site of another LOOP board, which includes the story of Sir Walter Raleigh laying down his cloak over a puddle for Elizabeth I:

![Section 17 LOOP market at Forty Hall Country Park](/images/posts/londonloop/s17marker_sm.jpg)

Section 17 ends in a built-up area around Enfield Lock.

Stats:
- Travel time to start: 0h 35m
- Walking time: 7h 52m
- Travel time from end: 1h 15m
- Travel cost: £8.70


## Day 8: Sections 18-21, Enfield Lock to Harold Wood (21.1mi / 34km)

Despite starting out in built-up Enfield Lock, section 18 soon has you up in the hills with views of London. I nearly missed one of the best views, from Yate's Meadow, because I started out following the main path in the forest rather than the recommended alternative route on the edge of the meadow itself:

![View from Yate's Meadow](/images/posts/londonloop/s18viewfromyatesmeadow_sm.jpg)

Shortly after section 19 starts at Chingford, there's Queen Elizabeth's Hunting Lodge (that's Queen Elizabeth I rather than II), and the Epping Forest Visitor Centre. It is also where the Epping Forest Centenary Way (now known as the Epping Forest Big Walk) intersects the LOOP. I've walked the Centenary Way a few times, so had passed this point a few times before.

Further on, the LOOP crosses the M11. That's another route into and out of London that I'm quite familiar with. Driving down the M11, there's a spot where the trees part to reveal the tall buildings of central London, which is nice to experience, and I'd often wondered where that place was. I think it must be just up from the Chigwell Services, where the LOOP crosses the M11.

Section 20 starts at Chigwell, where you pass the former Ye Olde Kings Head pub (which is now a restaurant). Apparently it was the inspiration for the Maypole Tavern which plays a central role in Dickens's Barnaby Rudge.

In Hainault Forest Park there is another view of London:

![View from Hainault Forest Park](/images/posts/londonloop/s20viewfromhainaultforestcountrypark_sm.jpg)

There are a few more other viewpoints, which I think must be the hills you can sometimes see to the east when at a high point in central London. Havering Country park also includes a number of [redwood trees on Wellingtonia Avenue](https://www.redwoodworld.co.uk/picturepages/havering.htm), before reaching the picturesque village of Havering-atte-Bower which appears at no 18 on the [List of highest points in London](https://en.wikipedia.org/wiki/List_of_highest_points_in_London).

I would have ended this day's walking at Havering-atte-Bower, but it would have taken around an hour (40 mins wait for the next bus, followed by 20 mins on the bus) just to get to the nearest train station. Since I'd also have risked more than an hour extra travelling at the start of the next day (given busses are only once an hour), I decided it made sense to spend another 2 hours walking section 21, from Havering-atte-Bower to Harold Wood (where the route passes an Elizabeth Line station).

This section also includes some fairly rural countryside. Shortly after departing Havering-atte-Bower, there's a field with some very old rusting iron posts:

![The Pyrgo gates](/images/posts/londonloop/s21pyrgogate_sm.jpg)

These are the Pyrgo gates, which are the last remnants of Pyrgo House where King Henry VIII's daughters Mary and Elizabeth played as children.

Beyond this, I passed some amazing hedgerows laden with all sorts of ripe fruit (it was mid August), which was apparently wild and going to waste. The plums were absolutely delicious.

Saw some more deer on the edge of Harold Hill, not long before reaching the end of section 21 at Harold Wood.

This was perhaps my favourite of all the days. 

Stats:
- Travel time to start: 1h 20m
- Walking time: 8h 10m
- Travel time from end: 1h 15m
- Travel cost: £9.00


## Day 9: Sections 22-24, Harold Wood to Purfleet (14.1mi / 22.7km)

Section 22 starts at Harold Wood, after which is the easternmost part of the loop, just north of Upminster.

Section 23 starts at Upmister, where I took a short detour to see Upminster windmill. The route goes through Hornchurch Country Park, where there are the remains of Sutton’s Farm WWII airfield (later RAF Hornchurch), which include fortifications such as concrete pillboxes and turrets. Shortly after the park, I took a short diversion to Ingrebourne Hill, which I'm glad I did for the last nice view towards central London:

![View form Ingrebourne Hill](/images/posts/londonloop/s23viewfromingrebournehill_sm.jpg)

At the end of section 23 in Rainham, I stopped for lunch in a pub. It was £3.25 for a really nice smooth pint of John Smiths! I've paid more than twice that for some overly hopped and carbonated craft beer in central London.

Soon after the start of section 24 in Rainham, you reach the river, and can start looking back across to where you started all that time ago. Unfortunately, on most of this stretch, to your left, there is a giant mound of smelly rubbish. It's the Rainham landfill, which is in active use. Just after the Rainham landfill, there was some kind of industrial estate where my walking on the public footpath appeared to trigger a loudspeaker with a booming warning that I was tresspassing and I was being recorded and must turn back immediately or face prosecution. I didn't, because I was on a public footpath.

Finally, after 9 days of walking, I reached the end at Purfleet. Somewhat anti-climactically, there doesn't appear to be any kind of marker or sign to mark the end. 

Stats:
- Travel time to start: 1h 10m
- Walking time: 6h 30m
- Travel time from end: 1h 40m
- Travel cost: £15.70


## Conclusion

So there you have it, the London LOOP completed. It was a nice hike, in mostly countryside-like surroundings with some small hills, which didn't entail a multi-day trip outside London. I know it isn't quite the same as [climbing the three peaks](/posts/climbing-the-three-peaks-snowdon-scafell-pike-and-ben-nevis/) or "bagging" Munros on weekends, but it is better than nothing.

Connected with quite a bit of history on the way, with Henry VIII and WWII featuring particularly heavily:
- Hall Place built in the reign of Henry VIII (section 1), Henry VIII's hunting ground at Nonsuch Park (section 7), another Henry VIII hunting ground at Bushy Park (section 9), Queen Elizabeth (I)'s Hunting Lodge which was actually built for King Henry VIII in 1543 (section 19), and Pyrgo House home for King Henry VIII's daughters Mary and Elizabeth (section 21).
- Kenley Airfield (section 5), Bentley Priory (section 15), and former site of Sutton's Farm airfield (section 23).

I also feel it has given me a much clearer mental map of the city's limits and its position in the landscape. Before I moved to London, I always had this idea that it simply stretched on as far as the eye could see in all directions. After living here for a while, I became aware that there were some vantage points from where you can look out and see (on very clear days) some distant green covered hills on the horizon in some directions particularly to the South and East, and conversely there were some points outside London where it is possible to look in and see some tall buildings in the distance. Plus, after getting a car, I've driven on many of the major routes into and out of the city, watching the gradual transition between countryside and city. But the actual location of these city limits remained slightly mysterious. Now I've walked many of those hills and looked back towards London, and walked under/over the main roads in/out, I feel I do have a better understanding of its geography. It is big, but not endless.

Many thanks to all the volunteers who helped plan and mark the route, and those who help to maintain it.

Summary stats:
- Walking time: 62h 30m 
- Travel time: 25h 10m
- Travel cost: £115.55
- Windmills: 2
- Henry VIII related landmarks: 5
- Golf courses: Lots




[^note1]: Cuckoos are a great example of how nature can be pretty unpleasant: "The chick will roll the other eggs out of the nest by pushing them with its back over the edge. If the host's eggs hatch before the cuckoo's, the cuckoo chick will push the other chicks out of the nest in a similar way." (source: [https://en.wikipedia.org/wiki/Common_cuckoo](https://en.wikipedia.org/wiki/Common_cuckoo)). Something to think about when listening to ["Everything is beautiful but us"](https://www.youtube.com/watch?v=07XHKXhfah0) (which is a great song BTW).

[^note2]: I'd often wanted to visit Banstead, given there was a period when the TFL Route planner always tried to send me there whenever I typed in Hampstead (presumably because Banstead Rail Station was a closer match than Hampstead Underground Station or Hampstead Heath Rail Station).

[^note3]: I later found out that this is the Crown and Treaty pub referenced in point 23 of the [Count Binface minifesto](https://www.countbinface.com/manifesto): "23 The hand dryer in the gents' urinals at the Crown & Treaty, Uxbridge to be moved to a more sensible position". Had I known that at the time I could have popped in to see exactly where the hand dryers were positioned (I did hear that they had already been moved, but can't confirm).
 
[^note4]: I like big infrastructure projects in principle, but in practice the HS2 does seem to be an expensive missed opportunity at the moment, especially if it does end up terminating at Old Oak Common rather than the more central Euston. Perhaps it would have been better to connect up all the cities in the north of England with high speed rail first. 

[^note5]: I think it is because, when I was a child, I read a news story about a passer-by getting killed by a stray golf ball, which hit them on their temple on their head.

