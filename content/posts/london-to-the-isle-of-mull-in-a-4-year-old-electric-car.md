---
title: "London to the Isle of Mull in a 4 year old electric car"
date: "2023-09-24"
lastmod: "2023-09-24"
tags: ["electric car", "tesla", "london", "isle of mull", "scotland"]
keywords: ["electric car", "tesla", "london", "isle of mull", "scotland"]
description: "This is a kind-of followup to 'London to Orkney, and most of the NC500, in an electric car.'"
categories: ["electric car"]
featured: false
draft: false
---

## Introduction 

This is a kind-of followup to [London to Orkney, and most of the NC500, in an electric car](/posts/london-to-orkney-and-most-of-the-nc500-in-an-electric-car/) from August 2020.

I had thought that the main themes would be how much the battery had degraded in 3-4 years, and how much busier the charging points were, but actually neither proved to be especially problematic.


## Summary

### Projected range

A common concern I've heard from non-EV-owners is battery degradation, i.e. how the maximum range per charge decreases over time. Some people seem afraid car batteries will be like their mobile phone batteries, where within a few years it'll have degraded to the point of being useless.

I have been collecting stats on the projected range for my car since Aug 2020, via the excellent [TeslaMate](https://docs.teslamate.org/). Since then, the highest projected range has been 311.54 miles (in Aug 2020), and lowest 288.28 miles (near to now):

![Projected range and mileage, 8 Aug 2020 to 10 Sep 2023](/images/posts/londontoisleofmull/projectedrange20200808-20230910.jpg)

Unfortunately I don't have stats from the point when I bought the car in Aug 2019, but I think the projected range was 320 miles. Assuming it was, that's a drop of around 10% in just over 4 years. Certainly better than a mobile phone. Also, I'd heard that the biggest falls are in the first 2 years, and this does seem to match that.

So while for the London to Orkney journey I planned no more than 200 miles between charges (for reasons outlined in [planning the route](/posts/london-to-orkney-and-most-of-the-nc500-in-an-electric-car/#the-route)), for this journey I planned 180 miles between charges, which turned out to be fine.

Note that I have been taking reasonably good care of the batteries, rarely charging over 90% or letting fall below 10%. In contrast, I know of people who have their electric cars on lease, and routinely charge to 100% and use until 0%, saying that the battery isn't theirs so they don't mind what happens to it (as an aside, I would be especially cautious of buying a second hand electric car that has been on lease).

### Route

![Route from London to Isle of Mull](/images/posts/londontoisleofmull/route.jpg)

### Charging & cost

These are the charging stats:

| Start Date | Location             | Charger type         | Driven (mi) | % Start | % End | Speed (kW) | Charged (kWh) | Price per kWh | Total cost |
| ---------- | -------------------- | -------------------- | ----------: | ------: | ----: | ---------: | ------------: | ------------: | ---------: |
| 2023-07-21 | London               | Trojan Energy        |             |         |   90% |            |               |               |            |
| 2023-07-22 | East Midlands Outlet | Tesla Supercharger   |         135 |     35% |   90% |       88.4 |          41.0 |         £0.40 |     £16.40 |
| 2023-07-22 | Kirklees             | Domestic 3-pin       |          55 |     69% |   73% |        2.0 |          2.74 |               |            |
| 2023-07-23 | Charnock Richard     | Tesla Supercharger   |          51 |     44% |   90% |       79.8 |          33.0 |         £0.38 |     £12.54 |
| 2023-07-23 | Abington Services    | Tesla Supercharger   |         157 |     20% |   90% |       57.0 |          50.0 |         £0.38 |     £19.00 |
| 2023-07-23 | Fort William         | Tesla Supercharger   |         167 |     30% |   90% |       92.3 |          43.0 |               |            |
| 2023-07-25 | Fort William         | Tesla Supercharger   |          39 |     71% |   90% |       31.1 |          16.0 |         £0.40 |      £6.40 |
| 2023-07-27 | Fionnphort           | Chargeplace Scotland |          93 |     57% |   90% |       38.8 |         30.56 |         £0.27 |      £8.25 |
| 2023-07-29 | Ballachulish Hotel   | Chargeplace Scotland |         102 |     45% |   83% |       41.9 |         32.21 |         £0.38 |     £12.24 |
| 2023-07-29 | Abington Services    | Tesla Supercharger   |         130 |     30% |   90% |       58.9 |          42.0 |         £0.39 |     £16.38 |
| 2023-07-29 | Tebay Southbound     | Tesla Supercharger   |          93 |     46% |   90% |       79.9 |          33.0 |         £0.43 |     £14.19 |
| 2023-07-29 | Moto Hilton Park     | Tesla Supercharger   |         143 |     32% |   90% |       86.7 |          42.0 |         £0.43 |     £18.06 |
| 2023-08-04 | London               | Trojan Energy        |         126 |     39% |   90% |       11.7 |        38.355 |         £0.45 |     £17.26 |


Total cost was £140.72 for 1292 miles. Although that is quite a bit more than the £17.72 for 1857 miles in August 2020, the original trip was using free Tesla supercharger credits from the referral programme[^note1] and the still mostly free-of-charge-at-the-time [ChargePlace Scotland](https://chargeplacescotland.org/) (CPS) chargers.

Although most of the charging points were fairly busy, I didn't have to wait for any of these to become free.


## Road trip report

The trip began on Saturday 22 July 2023.

### Day 1: London to the North of England

We were able to break the journey to Scotland with an overnight stopover at my sister-in-law's in the North of England.

### Day 2: North of England to Fort William

Near the start of day 2 we passed the highest point on a UK motorway (the [M62 Summit](https://en.wikipedia.org/wiki/M62_motorway#/media/File:M62_Summit_sign_29_July_2017.jpg) at 372m). Couldn't see much because the rain was so heavy.

Decided to stop and charge at Charnock Richard, in part because I always feel Charnock Richard is a turning point, with the roads, traffic and scenery all getting better as you head north.

On the way to the Abington superchargers, the car kept on advising that we take lengthy detours to other chargers, but we ploughed on regardless. When we got there we were lucky and got a place right away. It was the busiest stop on the whole trip though, and by the time we left there was a queue of at least 3 cars waiting (and no clear place to queue, so lots of getting out of cars and making verbal agreements). If Abington had been full I might have been tempted to try one of the large number of shiny new-looking and mostly vacant Applegreen chargers. Good job I didn't though - I found out later that they were £0.79 per kWh, so over twice the price of the Abington superchargers and way more expensive than any other charging the whole time even including the public charging at the end of the trip in London!

While charging at the Abington superchargers, the car kept notifying us that we had "enough charge to continue your trip". We ignored this, and continued charging to our planned 90% charge, intending to use this to get all the way to the Fort William supercharger. It is just as well we did, because we later found that the A82 to Fort William was closed from Tyndrum due to an accident, requiring an unplanned 24 mile detour via the A85.

### Days 3-4: Fort William

We stayed in a cabin by the shore of Loch Linnhe, near Fort William:

![Our accommodation by the shore of Loch Linnhe, near Fort William](/images/posts/londontoisleofmull/day5-lochlinnhe20230726_med.jpg)

No charging on site, but we were only a few miles from the Fort William supercharger.

The first full day in Fort William involved an early-ish start and a full day [hike to the top of Ben Nevis](/posts/climbing-the-three-peaks-snowdon-scafell-pike-and-ben-nevis/#ben-nevis-24-jul-2023) by myself and my oldest child, which we were both pretty pleased with.

### Day 5: Fort William to Isle of Mull

A 90% charge at Fort William was enough to get to the Isle of Mull. We had planned on a charge at the CPS charger by the Craignure ferry terminal, but saw another car try and fail to charge (perhaps because they didn't have an NFC card), so we didn't try.

It was a bit of a surprise to then find that most of the 20 miles from the Craignure ferry terminal to the campsite was single track road with passing places. With a name like A849 I had assumed 2 lanes, and so thought the 20 miles from the campsite back to the CPS charger would have been a lot quicker and easier than it actually was. The campsite was nice though:

![Camp site parking at Pennyghael, Isle of Mull](/images/posts/londontoisleofmull/day5-lochscridain20230728_med.jpg)

One night there was an electric BMW blocking a path in the campsite for several hours as they charged from an Electric Hook Up. Not sure if they'd got permission, and/or whether they had any range anxiety given the single track road to the nearest charger.

### Days 6-7: Isle of Mull

The highlight of the first full day on the Isle of Mull was a boat trip to Staffa and [Fingal's Cave](https://en.wikipedia.org/wiki/Fingal%27s_Cave), which departed from Fionnphort. I remembered studying the Mendelssohn composition inspired by Fingal's Cave in school, and we tried to listen to it in the car on the way, but the mobile signal wasn't up to it (much to the children's relief).

There was a CPS charger near the harbour at Fionnphort, so we charged there:

![Chargeplace Scotland at Fionnphort](/images/posts/londontoisleofmull/day6-chargingatfionnphort20230727_med.jpg)

No issues charging with the NFC card. Lots of reviews mentioning issues though, perhaps from people without NFC cards. Interestingly I did notice that the CPS app said the charger was available while I was charging. Thanks to having a decent car battery capacity, this was the only charge we needed on the island.

Spent the next day by Ardalanish Beach.

![Near Ardalanish Beach, with a view to the Paps of Jura](/images/posts/londontoisleofmull/day7-1ardalanish20230728_med.jpg)

With its crystal clear waters and golden sands I said it is like a beach on a Caribbean island, although my wife helpfully drew attention to the lack of palm trees.

Passed some windswept landscapes, dotted with abandoned crofts (presumably from the [Highland Clearances](https://en.wikipedia.org/wiki/Highland_Clearances)), that reminded me of where I spent my early childhood years.

![Derelict croft between Ardalanish Beach and Bunessan](/images/posts/londontoisleofmull/day7-2nearbunessan20230728_med.jpg)

### Day 8: Isle of Mull to London

Despite booking a few weeks in advance, there weren't any places on the Craignure to Oban ferry until the evening of Sat 29 Jul, which wasn't so good for our return journey, so we booked instead for the smaller Fishnish to Lochaline ferry. That meant an extra 40 miles (mostly single track) and an additional ferry (Ardgour to Corran), but meant we could make a much earlier start. Again we could have charged at the ferry terminal, but were comfortable with remaining range, so decided to just press on.

First charge stop was after all the single track roads and ferries at Ballachulish, near Glencoe:

![Chargeplace Scotland at Ballachulish Hotel, with view towards Glencoe](/images/posts/londontoisleofmull/day8-chargingatballachulishhotel20230729_med.jpg)

We had planned on stopping over somewhere random for the night on the way back, but we were lucky with ferries and traffic and ended up driving it all in one stretch. That was 14.5 hours total (11.5 hours driving + 2 hours charging + 1 hour other breaks), to cover 555 miles (including around 50 miles on single track road with passing places, and two ferries, at the start). Quite a trip.


## Lessons

### Have an NFC card for ChargePlace Scotland

This was one of the main lessons from the last trip, and looking at all the issues people were reporting on charging maps like ZapMap it still seems to be very much a problem, presumably because of mobile signal issues. I didn't have any problems with my NFC card though.

### Take your car's charging recommendations with a pinch of salt

If it says you have "enough charge to continue your trip", that assumes no lengthy detours. Also, if it says "your charger is busy, please consider rerouting to a less busy charger", it might suggest a new route which would add considerable time to your journey for potentially no benefit.

### Charge when it is convenient, not when you have no option

I've noticed some people only charge when they are low on charge. That is fine for very long and predictable journeys, but when you are based in the same general area, e.g. on holiday or at home, then charge when convenient rather when you are about to run out.


[^note1]: My current referral link is [https://ts.la/michael93471](https://ts.la/michael93471) for £500 or $500 off.

