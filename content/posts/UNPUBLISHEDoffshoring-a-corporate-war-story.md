---
title: "Offshoring: A corporate war story"
date: "2024-12-06"
lastmod: "2024-12-06"
tags: ["work", "offshoring", "management"]
keywords: ["work", "offshoring", "management"]
description: "This is a true (although hard-to-believe) corporate war story which I’ve told many many times over the years"
categories: ["work stories"]
featured: false
draft: true
---


This is a true (although hard-to-believe) corporate war story which I've told many many times over the years, typically in the context of "if you think this situation is bad, let me tell you about something a lot worse". Disclaimers: I think it is safe to share in public now that 20 years have elapsed[^note1], some details have deliberately been kept vague for obvious reasons, and I'll try to keep the tone professional but apologies if there are slight lapses (even after all this time it is still difficult to write about).

## Prologue: Trust and reputation take a long time to build

To set the scene, I was team lead in a department of around 30 developers and managers, at a large international investment bank. I had been there a few years, and had worked extremely hard to build a top performing and talented team, extremely satisfied customers, and a great reputation[^note2]. The IT management team were originally well respected, and valued people with skills and experience[^note3]. Unfortunately they then went through a rapid series of transitions, and by the time of this story they were largely under the influence of one of the well-known so-called "management consultancies".

## How not to do offshoring

Presumably egged on by the "management consultancy", the new senior IT management team became interested in "offshoring"[^note4] IT work, with my department chosen as the first one in the company to be "offshored". I had no input into the process, and I suspect my line manager didn't either. To put it politely, many suboptimal decisions were made, listed below roughly in the order they were encountered (rather than order of importance).

### Discourage key people from relocating

Firstly, everyone in the team was offered the same standard and very unattractive "relocation package", rather than some key people being offered a compelling package. We were each called into a room and given a sheet of paper which showed a massive pay cut, and some calculations suggesting that the lower taxes and cheaper cost of living in the offshore location would mean we would have the same standard of living. Perhaps unsurprisingly, only one person out of the 30 or so accepted this offer, with that one person being a recent graduate who had plenty of freedom and very little to lose, rather than the more experienced and established people who would have been more valuable as team leads in the offshore location. Those who did not accept the offer were told that we would almost certainly have our roles "offshored" within a very short space of time[^note5].

### Minimise communication and planning to maximise disruption

Secondly, the actual "offshoring" was performed in relative secrecy, and very quickly, so no-one was able to plan much in the way of transition, let alone handle clients and business sponsors effectively. Key details such as how many people would be "offshored" and when they would be "offshored" were not known, except presumably by senior management and HR. Furthermore, when someone was "offshored" they were immediately escorted off the premises with all access to the building and systems terminated, with this happening to everyone over the space of just a few days. On one of those days my manager asked me to learn as much as I could about one of the other teams in his area, without the head of that team finding out I was doing so, because he suspected that the head of that team would be "offshored" the next day. When the next day came, he was indeed "offshored" and I was made new head of that team, at which point I found out my first role as the new head of that team was to "offshore" all the members of that team. And a few days later my manager was "offshored" himself[^note6]. Within a few days, I was sitting in row after row of empty desks[^note7]. My main job became dealing with irate business sponsors who were seeing the service and support levels plunge. It is hard to convey in words the sense of complete chaos, not to mention utter despair, that this created. The excellent reputation and team I had spent so many years working so hard to build was being completely destroyed in a matter of days by forces outside of my control.

### Ensure knowledge transfer is impossible

Thirdly, and this is one of the most astonishing decisions of them all, each person had to be "offshored" (i.e. leave immediately without trace) to free up the "headcount" before their replacements could be hired in the offshore location. As pretty much anyone with any experience of running a knowledge-based and/or skills-based team would spot immediately, this meant it was impossible to do any effective knowledge transfer. Within the space of a few days, there were only 4 people left (including the one person who had agreed to relocate) from the original department of around 30 people. The only knowledge therefore available for transfer was the knowledge within those 4 people, which of course was a tiny fraction of the original knowledge held by 30 people. Note that those 4 people still had to support and maintain all the systems that were supported and maintained by around 30 people a few days earlier, in addition to trying to hire the new remote teams. The process of hiring the new teams, understandably, took many months. Again it is hard to convey in words how difficult this time was for the 3 people remaining (and the 1 relocating). I remember describing it to someone at the time as "like being on a rollercoaster, but without the up bits".

### Remove top talent

Fourthly, and this is almost a footnote although it shouldn't be, the company did nothing to try to retain any of the top talent that was being "offshored". Those who were "offshored" included many top performers with some excellent skills, but they were not offered new roles elsewhere in the firm, they were simply immediately escorted out of the building.

### Offshore systems which are unsuitable for offshoring

Fifthly, no considerations had been made as to which systems were being offshored, and some were quite unsuitable for offshoring. For example, one of the systems I inherited had physical devices in the head office used by most staff there, but the offshore office location used very different devices for the same purpose, so when one day they failed in the head office the offshore support team had absolutely no idea what they were working with. Needless to say, a lot of non-IT people got very upset with the IT department very quickly. And of course it was me, the one with a formerly excellent reputation, who had to take all the flak, rather than those responsible for the decisions that had led to that point.

### Have no plan for what to do after all the knowledge and talent is gone

Sixthly and finally, and this is perhaps the most incredible aspect of all, there seemed to be no plan as to what the new department in the offshore location would look like. The 3 of us left in the head office were left largely to ourselves because our manager had been "offshored" and none of us had any lines of communication left with the new senior management above (who in turn had no apparent interest in what was happening on the ground). So we just did what we knew best, which was to hire developers. We soon realised that many more developers were needed than the number of developers which they replaced, due to the issues with working remotely and lack of knowledge transfer. So we ended up hiring around 60 developers over the space of several months to replace the original 25 or so. We then discovered that 60 offshore developers needed quite a lot of local management, so we hired around 20 offshore managers to manage the 60 offshore developers. We also soon discovered that the 3 of us left in the head office could not manage around 80 people in the offshore location, so the head office management team was increased to around 20 people. This meant that the head office team was almost as large as it was before the "offshoring", but more expensive management resources rather than development and support.

## Short term "benefits"

To summarise the short term "benefits":

### Triple costs

The initial justification for the "offshoring" was cost saving, but the total headcount-related costs more than tripled, given that around 30 people in head office were replaced by around 100 people in total, including around 20 managers in head office who cost around the same as the 30 mostly developers they replaced.

### Lower quality

The quality and quantity of output was also lower for many years, while the new team slowly got up-to-speed.

### Destroy reputations 

To put it politely, clients and business sponsors were not happy, and to be completely frank, it made IT look pretty incompetent to non-IT people. On a purely personal level, the great reputation which I'd worked so hard to build over several years was irrevocably destroyed within a few weeks by senior management who were completely isolated from the consequences of their actions.

### Remove top talent

Top talent had not just been lost, but had been deliberately removed from the company. In at least one case a developer even completely left the IT industry[^note8].

## Long term benefits

### None (offshore the offshore team 13 years later)

One often heard apology (presumably popular because it wasn't falsifiable at the time) was that it was a "long term play" to "improve scalability" or something like that. But with the benefit of hindsight, even that turned out to be incorrect. Within 5 years the cost of hiring in the offshore location was as expensive as head office (due to factors such as the relatively small talent pool[^note9] and adverse exchange rates), and 13 years later (when the offshore team were finally starting to work really well) the entire offshore team were themselves "offshored" to a completely different country. And so the cycle began all over again.

## Anything can be a success with the right success criteria

Whichever way you look at it, none of this sounds like a success story. In fact it is almost impossible to think of any positive outcome[^note10]. However, the official success criteria was presented as "percentage of employees in high cost vs medium cost locations". Given the offshoring had changed the balance from 100% in a high cost location to 20% in a high cost location, it was deemed as a great success by senior management, and one of the people I suspect claimed responsibility was promoted to Managing Director (and pretty quickly left the firm)[^note11].

## Trying to make sense of it all

I normally try to end stories on a positive note, summarising the lessons learned or actions for the future or whatever, but it is a little difficult in this case. The best alternative I can think of is to offer various interpretations of the story.

### The dangers of moral hazard

"Moral hazard" is when people are rewarded for taking big risks while not having to face any negative consequences of their actions. Probably don't need to say much more about this, given how significant a role it played in this story, and how important a part it played in the 2007–2008 financial crisis just a few years later.

### Large companies are often self harming

Large companies often promote those who actually damage the company the most. I think this is because, firstly, many "successful" individuals achieve their "success" by doing what is best for them rather than the company and in many cases those are in direct opposition (e.g. senior managers seeking promotion by diverting resources into pointless vanity projects and away from genuinely beneficial but less visible projects), and secondly, in a sufficiently large company an individual's success is largely disconnected from a company's success.

### Class struggle and the exploitation of labour in a modern day corporate environment

There was very little socio-economic diversity in IT departments in the finance sector at the time. Most of the (IT) workers were predominantly state educated and the (senior IT management) ruling class were almost exclusively privately educated. So it would be possible to make a case that this was a modern day equivalent of the "lions led by donkeys" in the First World War, or the industrial revolution era coal miners doing all the dirty and dangerous work while the landed gentry simply reaped the profits from a safe distance. This deeply-ingrained subservience to the over-confident and under-qualified from the "old boy network" does explain something which may appear very strange to outsiders - why did those who remained feel powerless to fix the very obvious and avoidable problems, or at the very least why did they not simply resign? Might be worth exploring this angle in a lot more depth at some point.

### Trust and reputation take a long time to build but a short time to destroy

This is a point that runs throughout the story, and the negative effects weren't just for individuals, or a team, or a small department, but it made a whole IT department (with several thousand individuals) look bad in the eyes of non-IT people. The untold followup is of course how difficult it is to recover from such complete reputational destruction.

## Epilogue: This is about people's lives, livelihoods and health

I know I've tried to keep the writing very business-like, but it has been hard even now after two decades. This shouldn't just be a story about mismanagement, it should also be a story about the people impacted, and the whole ethical and human aspect. Incompetent management isn't just bad for the company, it is bad for all the people affected by it. Unfortunately it isn't an isolated case either - although this is perhaps my most extreme story, I do have many more almost-as-bad stories.


[^note1]: It isn't exactly a government document, but if it was, the [30 year rule has changed to a 20 year rule](https://en.wikipedia.org/wiki/Thirty-year_rule#Change_to_a_twenty-year_rule).

[^note2]: My achievements included introducing the first major open source software to the bank, and running a high profile project which involved 1-2-1s with the head of IT.

[^note3]: The head of IT even said once that they liked to surround themselves with people smarter than they were, which I still feel is a great thing for leadership to aspire to. I used to joke that the environment was originally "smart people trying to get good stuff done" but then changed into "not so smart people trying to stop good stuff getting done".

[^note4]: "Offshoring" is setting up a team/department within the same company but in an overseas (usually lower cost) country, as opposed to "outsourcing" which is transferring the work to another company.

[^note5]: Although no-one was allowed to use the term at the time, for sake of clarity I can now confirm that being "offshored" was a euphemism for being made redundant.

[^note6]: In common with almost everyone who was "offshored", I've not seen or heard from my former manager since, so unfortunately don't know how well he fared afterwards.

[^note7]: I remember at one point going for a coffee in the coffee area, and looking at the health and safety information board, and realising that all the designated first-aiders and fire evacuation personnel were all gone.

[^note8]: One Java developer retrained as a plumber because "at least plumbing can't be offshored".

[^note9]: We eventually ended up having to recruit in other countries thousands of miles away and relocate them to the offshore location.

[^note10]: I later found out about [regulatory arbitrage](https://en.wikipedia.org/wiki/Arbitrage#Regulatory_arbitrage), which is sometimes used to make wholesale IT staffing changes appear cost effective. But I don't know if it was in the minds of senior management at the time, plus it is usually used for "outsourcing" rather than the "offshoring" described here. Even if it was, it is more of an accounting trick rather than a real benefit.

[^note11]: This is a management pattern which is so common I even heard a term for it (unfortunately I don't recall what that term was, but it was something along the lines of "smash and grab management"). The pattern is - a new senior manager comes in, introduces major changes (not necessarily because they're needed, but simply because that's what they do), picks up a big reward for their bold changes, and then leaves before the consequences of their actions become known. This of course means that they cannot learn from any mistakes, but that is irrelevant for those following this pattern, given they are primarily driven by personal benefit, and don't plan on being around long enough to see any detrimental effects on the company and/or its people and/or its customers.

