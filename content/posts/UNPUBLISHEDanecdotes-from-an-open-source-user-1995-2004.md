---
title: "Anecdotes from an open source user 1995-2004"
date: "2024-10-22"
lastmod: "2024-10-22"
tags: ["open source"]
keywords: ["open source"]
description: "An account of an ordinary user's experience with using open source from 1995 to 2004."
categories: ["open source"]
featured: false
draft: true
---


## Introduction

First off, just to manage expectations, there are no great "I interacted with Linus Torvalds in 1992" type of stories here. This is just an account of an ordinary user's experience with using open source from 1995 to 2004. The motivation is simply to write up some of the anecdotes I've told over the years, perhaps so I can send links to them in future rather than repeat myself:-) Note that it focuses on using open source - contributing to open source would be a different (admittedly shorter) post.

Note also that while I like open source, I'm no zealot, generally preferring it for practical rather than philosophical reasons, but remaining open to closed source alternatives.


## Open source at work

### Anecdote 1: Using open source for a software company's web site, 1995-1998 

After finishing university in 1995, I got a job at a software company. I was keen to set up a web site for them, although that wasn't what I was hired to do, because I'd set up my first web site at university in 1994 and thought that this world wide web thing was going to be quite popular in future.

The company already had some of the prerequisites for a web site in place, from hosting their own email servers. They'd managed to secure a dot com domain, which was a big deal back then[^note1]. They'd also got a hugely expensive 64kb leased line to the building, and a Cisco router.

Although the software company was very successful (there were around 20 people when I joined, and around 200 when I left), the internet wasn't its business, so there wasn't really any budget for the web site. It was just two of us working on it in our spare time - I looked after the software side of things, while the other person looked after the hardware (that was not unusual - few, if any, people were actually paid to set up and run web sites in the UK at that time).

The hardware person chose Slackware Linux, on self-assembled hardware, to cut costs. I chose Apache httpd for the web servers, and used Perl for scripting. So it was all open source. The only alternative at that time would have been expensive Unix hardware[^note2], although beyond the OS the software stack would probably have been much the same.

It ran really well. By the time I left in 1998, the web site had some relatively advanced dynamic functionality (I developed an early content management system using Perl, had dynamic content via mod_perl CGI scripts and a MySQL database, and built an online software evaluation process, along with a mirror site in the US office for faster downloads), was peaking on 14 million hits in a day (the weekly "top 10 viruses" chart which I had the idea for was particularly popular, and the online software evaluation became the source of 95% of sales leads for the US office within its first year), and had no downtime at all (apart from when some building work outside London severed the UK's main internet connection, but that led to a UK-wide internet outage, not just for our site).

### Realisation 1: Something free can be better in every way than something expensive

When I was looking to leave the software company, I remember being slightly apologetic at job interviews about my exclusive use of open source software. I guess I felt a bit of a cheap-skate, and still had the assumption that free must be inferior to expensive.

My second job was in the finance sector. We had a mix of Solaris (which I had used at university) and Windows servers (which I was encountering in a professional setting for the first time), with proprietary software like Netscape (later iPlanet) Enterprise Server.

No issues with Solaris, apart from how eye-wateringly expensive it was. It was Windows servers which were the big shock. Firstly, they ran incredibly slowly in comparison to the Linux servers I'd run in my previous job (much higher spec servers really struggled to handle a tiny fraction of the traffic). Secondly, they were astonishingly unreliable (regularly crashing and needing rebooting on a frequent basis). Thirdly, they cost an order of magnitude more money (not just software licences but also requiring significantly higher spec hardware).

Practical experience of both open source and proprietary software had made me realise that sometimes free can be better than something expensive in pretty much every conceivable way. What I couldn't initially get my head around though was why so many people (myself included) had been duped into believing the expensive Microsoft solutions were better than the open source alternatives. Of course the reason was soon revealed via the [Halloween documents](https://en.wikipedia.org/wiki/Halloween_documents) and the revelation of "[embrace, extend, extinguish](https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish)".

### Anecdote 2: Discussing open source with senior IT executives in the finance sector in 1999

In 1999 my boss got invited to a fancy dinner for IT executives in the finance sector, but couldn't make it, so sent me along instead. It was at [Mosimann's private dining rooms](https://www.mosimann.com/) in Belgravia, and we were in the Fabergé Room, surrounded by cabinets containing (presumably replica) Fabergé eggs. I had never been anywhere like it before. I suppose now that I probably looked a little out of place, but I tried my best to fit in, and put on my best charity shop suit (I was very proud of it because it was nearly new and probably from the same decade, unlike most of my other charity shop clothes which were visibly 1980s or 1970s, although unfortunately it was very obviously several sizes too big).

At the table were a number of senior IT executives, and I remember the CTO or CEO of the UK's first internet bank was to my left. The conversation came around to Linux. I cited how great it was (from experience), how you can do things with open source that just aren't possible with closed source, e.g. I'd seen people compiling their own hyper-optimised kernels, not to mention how many people coming out of university would have experience of it, and so on. But I was the only one there who seemed to think it might have any future in the finance sector, and everyone else at the table remained completely unconvinced.

### Anecdote 3: Introducing open source to a global financial institution in 2004

By 2000 I had moved to another financial institution. Again it was a mix of Solaris and Windows. I didn't concern myself with Windows development much, although there was one big complex maintenance-heavy Microsoft ASP app I replaced with a few lines of largely maintenance-free Java code[^note3].

Our server-side Java apps were initially hosted on ServletExec, then Allaire JRun, then Borland Enterprise Server (BES). BES was a commercially supported version of Apache Tomcat, but it had a lot of significant issues. After a long campaign, I was able to convince the powers-that-be to allow us to deploy an application in production using the open source Apache Tomcat instead, so we didn't have to deal with all the issues of BES. The key argument had been that we (the development team) would support the application server (Tomcat) in the same way that we supported the code we had written. In May 2004 it went live. As far as I was aware, it was the first major open source product in production use in the large multi-national bank at which I worked.

Within a year or two the first Linux servers had also been deployed (unfortunately I can't claim any credit for those, because I was on the development side of the very large IT department rather than the infrastructure side). But needless to say, within a few years, open source in the finance sector became completely normalised. In fact in many cases open source became the preferred choice, although often simply because the new vendor onboarding and procurement processes for commercial software were so onerous.


## Open source at home

### Anecdote 4: What made me switch from WIndows to Linux at home, 2003

I began using Linux at home, on and off, from the late 1990s.

My turning point with MS Windows came in 2003. I was building an ultra-cheap second computer for use at home. The first shock was when I realised I had to fork out more than the cost of the hardware just to buy the Windows XP licence[^note4]. The second shock was when I tried to install it - the CD was so covered in holograms and copy protection that my ultra-cheap CD-ROM made horrible grinding noises every time it tried and failed to read it (in the end, even though I'd spent more money than the hardware on a legitimate software licence, I was forced to download a pirate copy just so I could burn it onto a CD which my CD-ROM could actually read). The third shock was when I hit the Microsoft Product Activation nonsense, which made Windows effectively unusable for someone building a PC and fiddling around with different configurations (the easy solution would have been to use the leaked corporate key with the pirate copy, but I prefer to do things legally and ethically, and I really wanted to use the legitimate licence that had cost more money than the hardware). To cut a long story short, in the end I just gave up and installed Linux.

### Realisation 2: Closed source puts profit above users and usability

Since moving to Linux at home in 2003, I've never looked back. Everything I've heard since suggests that Windows XP was as good as it ever got, and each major release after XP has been progressively worse from a technical end-user's perspective. As far as I can tell, it is simply because the primary company driver is to increase shareholder returns year on year, which means extracting increasing amounts of money from users. That model isn't necssarily incompatible with improving usability, but that seems to be the way it generally ends up with Big Tech. AKA "platform decay" or "enshittification".


[^note1]: If you weren't a US-based company you had to prove that you were an international company to get a dot com domain, so one of the founders got his brother to set up an office in his home country.

[^note2]: Microsoft had started out running microsoft.com from Unix servers and were still dismissing the internet as a fad.

[^note3]: One of the high maintenance aspects of the Visual Basic app had been admin screens to manage timezones and activate/deactivate daylight savings, because MS Visual Basic was completely missing any equivalent of java.util.date at that time. Again I just couldn't fathom why so many people so firmly believed that something so clearly so much worse was in actual fact better. Although I did later realise that most of the Microsoft marketing for around a decade or so was essentially "we know this version isn't as good as the competition, but the next version will be better", on repeat for each new version. Oh, and there were the super shady (to put it politely) deals with senior IT figures, but that is another story entirely.

[^note4]: All the components cost around £80, and I paid £95.95 for the OEM Windows XP licence on 29 Sep 2003.

