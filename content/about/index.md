---
title: "About"
date: "2019-11-08"
lastmod: "2024-10-23"
tags: ["michael lewis"]
keywords: ["michael lewis"]
description: "Welcome to my personal site. I am one of the many Michael Lewises."
categories: []
draft: false
---

Welcome to my personal site. I use it to keep notes on various side-projects and topics I find of interest, mainly for my own future reference. It is more functional than ornamental, so if it was a "[digital garden](https://searchmysite.net/search/?q=digital+garden)" it would be more of a vegetable patch than a floral display.

For my professional services site, see [https://aeliantech.com/](https://aeliantech.com/).


## About me

I am one of the many _many_ Michael Lewises. Not the one who writes finance books, or one of the three who used to work in the same building as me, or the one who used to live at the same house number on the adjacent street, so sorry to disappoint you if you're looking for one of those Michael Lewises.

Those who know me might be reassured that they've found the "correct" Michael Lewis via one or more of: "I", Shetland, Peebles, Edinburgh, London, artificial intelligence, internet, digital architect, father of 2.


## Topics

The list of topics on this site are currently (in alphabetical order):
- [artificial intelligence](/categories/artificial-intelligence/)
- [electric car](/categories/electric-car/)
- [hiking](/categories/hiking/)
- [internet](/categories/internet/)
- [media server](/categories/media-server/)
- [old photos](/categories/old-photos/)
- [open source](/categories/open-source/)
- [personal website](/categories/personal-website/)
- [plants](/categories/plants/)
- [search](/categories/search/)
- [today i learned](/categories/today-i-learned/)
- [writing](/categories/writing/)


## Contact

You could try to email me at myfirstname at mydomain, or very occasionally find me on the [fediverse](https://fosstodon.org/@michaellewis), or use the LinkedIn link on the home page.


## Social media

My last post on FB: 31 Jan 2012 "I am just logging out and may be some time".

