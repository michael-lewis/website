Source files for the website at [https://michael-lewis.com/](https://michael-lewis.com/).

Information on these files at [https://michael-lewis.com/posts/building-a-site-with-hugo-and-gitlab/](https://michael-lewis.com/posts/building-a-site-with-hugo-and-gitlab/).
